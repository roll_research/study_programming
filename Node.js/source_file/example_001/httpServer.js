var http = require('http');

http.createServer(function (req,res){
    var data = [];
    
    res.on('error',function(err){
        logger.log("event RES error : "+err);
    });
    
    req.on('err',function(err){
        logger.log("event REQ error: "+
                  err);
    });
    
    req.on('data',function(chunk){
        data.push(chunk);
        logger.log(" event data chunk length :"+chunk.length);
    });
    
    req.on('end',function(){
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.end('Hello World');
    });
}).listen(9080);