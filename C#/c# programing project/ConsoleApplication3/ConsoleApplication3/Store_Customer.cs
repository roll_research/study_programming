﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Store_Customer : Basic_Impormation
    {
        Stack<string> Manu_list = new Stack<string>();

        Queue<string> name = new Queue<string>();
        Queue<int> how_many = new Queue<int>();
        Queue<int> price = new Queue<int>();
        Queue<string> order = new Queue<string>();

        public void Insert_Customer(string Name,int How_many,int Price,string Order)
        {
            if (name.Count == how_many.Count && price.Count == order.Count && name.Count == price.Count)
            {
                name.Enqueue(Name);
                how_many.Enqueue(How_many);
                price.Enqueue(Price);
                order.Enqueue(Order);

                Console.WriteLine("정보가 추가 되었습니다.");
            }
        }

        public void Delete_Customer()
        {
            if (name.Count == how_many.Count && price.Count == order.Count && name.Count == price.Count)
            {
                string rename = name.Dequeue();
                int rehow_many = how_many.Dequeue();
                int reprice = price.Dequeue();
                string reorder = order.Dequeue();

                Console.WriteLine("[삭제된 정보] 이름: " + rename + " 인원 수: " + rehow_many + " 가격: " + reprice + " 주문: " + reorder);
            }
        }

        public void Total_Customer()
        {
            string[] Name_Array = new string[name.Count];
            int[] How_Many_Array = new int[how_many.Count];
            int[] Price_Array = new int[price.Count];
            string[] Order_Array = new string[order.Count];

            name.CopyTo(Name_Array, 0);
            how_many.CopyTo(How_Many_Array, 0);
            price.CopyTo(Price_Array, 0);
            order.CopyTo(Order_Array, 0);

            for(int i = 0; i < Name_Array.Length; i++)
            {
                Console.WriteLine("이름: "+Name_Array[i]+"인원수: "+How_Many_Array[i]+"가격: "+Price_Array[i]+"주문: "+Order_Array[i]);
            }
        }

        public void Finish()
        {
            name.Clear();
            how_many.Clear();
            price.Clear();
            order.Clear();

            ReSet_Store_Customer();
        }

        public void Insert_Manu_list()
        {
            //Manu_list.Push
        }
    }
}
