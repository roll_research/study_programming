# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 15:21:25 2020

@author: GM190524
"""

import seaborn as sns

df = sns.load_dataset('titanic')

print(df['embark_town'][825:830])
print()

#df['embark_town'].fillna(method='ffill',inplace=True)
df['embark_town'].fillna(method='bfill',inplace=True)
print(df['embark_town'][825:830])