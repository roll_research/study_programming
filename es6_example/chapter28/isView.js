let bufferObj = new ArrayBuffer(10);
console.log(ArrayBuffer.isView(bufferObj));

let typedObj = new Int16Array();
console.log(typedObj.isView(typedObj));

let viewObj = new DataView(bufferObj);
console.log(viewObj.isView(viewObj));