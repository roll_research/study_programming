# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:17:40 2020

@author: GM190524
"""

import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel('시도별 전출입 인구수.xlsx',fillna=0,header=0)

#print(df)

df = df.fillna(method='ffill')

mask = (df['전출지별'] =='서울특별시') & (df['전입지별'] !='서울특별시')
df_seoul = df[mask]
df_seoul = df_seoul.drop(['전출지별'],axis=1)
df_seoul.rename({'전입지별':'전입지'},axis=1,inplace=True)
df_seoul.set_index('전입지',inplace=True)

#print(df_seoul)

sr_one = df_seoul.loc['경기도']

#print(sr_one)

#plt.plot(sr_one.index,sr_one.values)

plt.plot(sr_one)
