# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 14:10:03 2020

@author: GM190524
"""
import pandas as pd

exam_dict= {'수학':[90,80,70],'영어':[98,89,95],
            '음악':[85,95,100],'체육':[100,90,90]}

df = pd.DataFrame(exam_dict,index=['서준','우현','인아'])

print(df)
print('\n')

df4 = df[:]
df4.drop(['수학'],axis=1,inplace=True)
print(df4)
print('\n')

df5 = df[:]
df5.drop(['영어','음악'],axis=1,inplace=True)
print(df5)
print('\n')

