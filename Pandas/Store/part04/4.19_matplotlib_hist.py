# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 12:23:27 2020

@author: GM190524
"""

import pandas as pd
import matplotlib.pyplot as plt

plt.style.use('classic')

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horespower','weight',
              'acceleration','model year','origin','name']

df['mpg'].plot(kind='hist',bins=10,color='coral',figsize=(10,5))

plt.title('HIstogram')
plt.xlabel('mpg')
plt.show()