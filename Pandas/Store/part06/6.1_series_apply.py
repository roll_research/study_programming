# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 17:34:56 2020

@author: GM190524
"""

import seaborn as sns

def add_10(n):
    return n + 10

def add_two_obj(a,b):
    return a+b


titanic = sns.load_dataset('titanic')
df = titanic.loc[:,['age','fare']]
df['ten'] = 10
print(df.head())
print()

print(add_10(10))
print()

print(add_two_obj(10,10))
print()

sr1 = df['age'].apply(add_10)
print(sr1.head())
print()

sr2 = df['age'].apply(add_two_obj,b=10)
print(sr2.head())
print()

sr3 = df['age'].apply(lambda x : add_10(x))
print(sr3.head())
print()