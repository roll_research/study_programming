# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 14:18:20 2020

@author: GM190524
"""

import pandas as pd

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horsepower','weight','acceleration','model year','origin','name']

print(df.head())
print()
print(df.tail())

print(df.shape)

print(df.info())

print(df.dtypes)
print()

print(df.mpg.dtypes)

print(df.describe())
print()
print(df.describe(include='all'))