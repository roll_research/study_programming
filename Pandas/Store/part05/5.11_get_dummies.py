# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 12:48:52 2020

@author: GM190524
"""

import pandas as pd
import numpy as np

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horsepower','weight',
              'acceleration','model year','origin','name']

df['horsepower'].replace('?',np.nan,inplace=True)
df.dropna(subset=['horsepower'],axis=0,inplace=True)
df['horsepower'] = df['horsepower'].astype('float')

count, bin_divider = np.histogram(df['horsepower'],bins=3)

bin_name = ['저출력','보통출력','고출력']

df['hp_bin'] = pd.cut(x=df['horsepower'],
                      bins=bin_divider,
                      labels=bin_name,
                      include_lowest=True)

horsepower_dummies = pd.get_dummies(df['hp_bin'])
print(horsepower_dummies.head(15))