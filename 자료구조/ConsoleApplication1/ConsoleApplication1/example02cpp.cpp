/*#include <stdio.h>

 typedef struct person {
	int age;
	char name[50];
};

void main() {
	 person a = {18,"오한성"};
	 person b = a;
}
*/

//자체 참조 구조체
/*typedef struct ListNode {
	char data[10];
	struct ListNode *link;
}ListNode;
*/

/*#include <stdio.h>

void main(){
	char a = 'A';
	char *b = NULL;
	b = &a;
	*b = 'B';
	printf("%c \n", *b);
}
*/

/*#include <stdio.h>

void swap(int *px, int *py) {
	int tmp;
	tmp = *px;
	*px = *py;
	*py = tmp;
}

void main() {

	int a = 1, b = 2;
	printf("swap을 호출하기 전: a= %d, b= %d \n", a, b);
	swap(&a, &b);
	printf("swap을 호출한 후: a= %d, b= %d \n", a, b);
}*/

/*#include <stdio.h>

void main() {
	struct {
		int i;
		float f;
	}s, *ps;

	ps->i = 2;
	ps->f = 3.14;
	(*ps).i;
	(*ps).f;
}*/

//#include <stdio.h>

//void main() {
	/*
	char *pc = NULL;
	*pc = E;
	*/

	/*
	int *pi;
	float *pf;
	pf = (float*)pi;
	*/

	/*size_t i = sizeof(int);
	struct AlignDepends {
		char c;
		int i;
	};
	size_t size = sizeof(struct AlignDepends);
	int array[] = { 1,2,3,4,5 };
	size_t sizearr =sizeof(array)/sizeof(array[0])
}*/

/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Example {
	int number;
	char name[10];
};

void main() {
	struct Example *p;

	p = (struct Example *)malloc(2 * sizeof(struct Example));
	if (p == NULL) {
		fprintf(stderr, "잘못 만들어졌습니다.\n");
		exit(1);
	}
	p->number = 1;
	strcpy(p->name, "오한성");
	(p + 1)->number = 2;
	strcpy((p + 1)->name, "오한주");

	printf("%d   %s \n", p->number, p->name);
	printf("%d   %s \n", (p+1)->number, (p+1)->name);
}*/