﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            bool Select = true;
            int number = 0;
            string name;
            int age;
            int ki;
            Person Person = new Person();
            imformation imfor = new imformation();

            while (Select) {
                Console.WriteLine("1.등장/ 2.퇴장 / 3.지금의 총 명 / 4.마지막 존재자 이름 / 나머지 숫자: 종료 ");
                number = int.Parse(Console.ReadLine());
                switch (number)
                {
                    case 1:
                        Console.Write("이름: ");
                        name = Console.ReadLine();
                        Console.Write("나이: ");
                        age = int.Parse(Console.ReadLine());
                        Console.Write("키: ");
                        ki = int.Parse(Console.ReadLine());
                        imfor = new imformation(name,age,ki);
                        Person.Enqueue(imfor);
                        break;
                    case 2:
                        imfor=Person.Dequeue();
                        Console.WriteLine(imfor.Get_Name() + " " +imfor.Get_Age()+" "+imfor.Get_Ki()+" ");
                        break;
                    case 3:
                        Person.Total_Customer();
                        break;
                    case 4:
                        imfor = Person.Peek();
                        Console.WriteLine(imfor.Get_Name() + " " + imfor.Get_Age() + " " + imfor.Get_Ki() + " ");
                        break;
                    default :
                        Console.WriteLine("종료");
                        Select = false;
                        break;
                }

            }
        }
    }
}
