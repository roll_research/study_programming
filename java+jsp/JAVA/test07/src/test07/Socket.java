package test07;

import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.Date;


public class Socket {

	static final int PORT = 8888;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	try{
		ServerSocket serverSocket = new ServerSocket(PORT);
		
		System.out.println("console> 서버 : 클라이언트의 접속을 기다립니다.");
		
		while(true){
			java.net.Socket Socket = serverSocket.accept();
			
			System.out.println("console> 서버 "+ Socket.getInetAddress()+"클라이언트와 "+Socket.getLocalPort()+"포트로 연결되었습니다.");
			
			try{
				OutputStream stream = Socket.getOutputStream();
				stream.write(new Date().toString().getBytes());
			}catch (Exception e){
				e.printStackTrace();
			}finally
			{
				Socket.close();
			}
			
		}
	}catch(IOException e){
		e.getMessage();
	}
		
	}

}
