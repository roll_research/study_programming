# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 13:52:06 2020

@author: GM190524
"""

import pandas as pd

df = pd.DataFrame([[15,'남','덕영중'],[17,'여','수리중']],
                  index=['준서','예은'],
                  columns=['나이','성별','학교'])

print(df)
print('\n')

df.rename(columns={'나이':'연령','성별':'남녀','학교':'수속'},inplace=True)

df.rename(index={'준서':'학생1','예은':'학생2'},inplace=True)

print(df)