﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label_real.BorderStyle = BorderStyle.FixedSingle;
            label_real.AutoSize = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string> ();

            foreach(var item in Controls)
            {
               if(item is CheckBox)
                {
                    CheckBox checkbox = (CheckBox)item;
                    if (checkbox.Checked)
                    {
                        list.Add(checkbox.Text);
                    }
                }
            }
            MessageBox.Show(string.Join(",", list));
        }
    }
}
