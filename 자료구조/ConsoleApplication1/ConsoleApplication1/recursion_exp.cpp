#include <stdio.h>

/*int factorial_iter(int n) {
	int k,v = 1;
	for (k=n; k >0; k--) 
		v =v*k;
		return (v);
}

int factorial(int n) {
	if (n <= 1) return 1;
	else return (n*factorial(n - 1));
}

void main() {
	int n = 5;
	int recusion_factorial=factorial(n);
	int real_factorial = factorial_iter(n);

	printf("%d \n", recusion_factorial);
	printf("%d \n", real_factorial);
*/

/*double slow_power(double x, int n) {

	int i;
	double r = 1.0;
	for (i = 0; i < n; i++)
		r = r*x;
	return r;
}

double power(double x, int n) {

	if (n == 0) return 1;
	else if ((n % 2) == 0)
		return power(x*x, n / 2);
	else
		return x*power(x*x, (n - 1) / 2);
}

void main() {

	double x = 4.0;
	int n = 5;

	printf("%f \n", slow_power(x,n));
	printf("%f \n", power(x, n));

}*/

/*int fib(int n) {

	if (n == 0) return 0;
	if (n == 1) return 1;
	return (fib(n - 1) + fib(n - 2));
}

int fib_iter(int n)
{
	if (n < 2)return n;
	else {
		int i, tmp, current = 1, last = 0;
		for (i = 2; i <= n; i++) {
			tmp = current;
			current += last;
			last = tmp;
		}
		return current;
	}

}

void main() {

	int n = 10;

	printf("%d \n",fib(n));
	printf("%d \n",fib_iter(n));

}*/

/*void hanoi_tower(int n, char from, char tmp, char to) {
	if (n == 1)printf("원판 1을 %c에서 %c으로 옮긴다.\n", from, to);
	else {
		hanoi_tower(n - 1, from, to, tmp);
		printf("원판 %d을 %c에서 %c으로 옮긴다. \n", n, from, to);
		hanoi_tower(n - 1, tmp, from, to);
	}
}
void main() {
	hanoi_tower(4, 'A', 'B', 'C');
}*/

