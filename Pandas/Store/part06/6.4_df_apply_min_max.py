# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 17:53:17 2020

@author: GM190524
"""

import seaborn as sns

titanic = sns.load_dataset('titanic')
df = titanic.loc[:,['age','fare']]
print(df.head())
print()

def min_max(x):
    return x.max()-x.min()

result = df.apply(min_max)
print(result.head())
print()
print(type(result))