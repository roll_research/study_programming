﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication20
{
    delegate string Callback();
    class Callback_Server
    {
        public event Callback Allkill;
        public Callback_Server() { Allkill = null; }
    }
    class Client
    {
        string name;
        public Client(string str) { name = str; }
        public void Die_Client()
        {
            Console.WriteLine(name + "-->dead...");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Client client;
            Callback_Server server = new Callback_Server();
            for(int i=0; i < 5; i++)
            {
                client = new Client(Console.ReadLine());
                server.Allkill += client.Die_Client; 
            }
            server.Allkill();
        }
    }
}
