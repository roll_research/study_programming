﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication19
{
    delegate void Callback();
    class Callback_Server
    {
        public Callback AllKill;
        public Callback_Server() { AllKill = null; }
    }
    class Client
    {
        string name;
        public Client(string str) { name = str; }
        public void Die_Client()
        {
            Console.WriteLine(name + "-->dead...");
        } 
    }

    class Program
    {
        static void Main(string[] args)
        {
            Client client;
            Callback_Server server = new Callback_Server();
            for(int i=0; i<5; i++)
            {
                client = new Client(Console.ReadLine());
                server.AllKill += client.Die_Client;
            }
            server.AllKill();
        }
    }
}
