var express = require('express')
, http = require('http')
, path = require('path');

var bodyParser = require('body-parser')
,cookieParser = require('cookie-parser')
,session = require('express-session')
,static = require('serve-static')
,errorHandler = require('errorhandler');

var expressErrorHandler = require('express-error-handler');
var expressSession = require('express-error-handler');

var mongoose = require('mongoose');

var crypto = require('crypto');

var database_loader = require('./database/database_loader');
var route_loader = require('./routes/route_loader');
var config = require('./config/config');

var socketio = require('socket.io');

var cors = require('cors');

var app = express();

app.set('port',process.env.PORT||3000);

app.use(bodyParser.urlencoded({ extended: false}));

app.use(bodyParser.json());

app.use('/public',static(path.join(__dirname,'public')));

app.use(cookieParser());

app.use(expressSession({
    secret:'my key',
    resave:true,
    saveUninitialized:true
}));

app.use(session({
    cookie :{maxAge:6000},
    secret : 'my key',
    resave : false,
    saveUninitialized:false
}));

app.set('views',__dirname+'/views');

app.set('view engine','ejs');

console.log('뷰 엔진이 ejs로 설정되었습니다.');

var passport = require('passport');
var flash = require('connect-flash');

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

var configPassport = require('./config/passport');
configPassport(app,passport);

app.use(cors());

var router = express.Router();
route_loader.init(app,router);

var userPassport = require('./routes/user_passport');
userPassport(router,passport);

var errorHandler = expressErrorHandler({
    static:{
        '404':'./public/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

var server = http.createServer(app).listen(app.get('port'),function(){
    console.log('서버가 시작되었습니다. 포트 :'+ app.get('port'));
    
    database_loader.init(app,config);
});

var io = socketio.listen(server);

var login_ids = {};

console.log('socket.io 요청을 받아들일 준비가 되었습니다.');

io.sockets.on('connection',function(socket){
    console.log('connection info : ', socket.request.connection._peername);
    socket.remoteAddress = socket.request.connection._peername.address;
    socket.remotePort = socket.request.connection._peername.port;
    
    socket.on('message',function(message){
        console.log('message 이벤트를 받았습니다.');
        console.dir(message);
        
        if(message.recepient =='ALL'){
            console.log('나를 포함한 모든 클라이언트에게 message 이벤트를 전송합니다.');
            io.sockets.emit('message',message);
        }else{
            if(login_ids[message.recepient]){
                io.sockets.connected[login_ids[message.recepient]].emit('message',message);
                
                sendResponse(socket,'message','200','메세지를 전송했습니다.');
            }else{
                sendResponse(socket,'login','404','상대방의 로그인 ID를 찾을 수 없습니다.');
            }
        }
    });
    
    socket.on('login',function(login){
        console.log('login 이벤트를 받았습니다.');
        console.dir(login);
        
        console.log('접속한 소켓의 ID : '+socket.id);
        login_ids[login.id] =socket.id;
        socket.login_id = login.id;
        
        console.log('접속한 클라이언트 ID 개수 : %d',Object.keys(login_ids).length);
        
        sendResponse(socket,'login','200','로그인되었습니다.');
    });
    
    function sendResponse(socket,command,code,message){
        var statusObj = {command : command, code : code, message : message};
        socket.emit('response',statusObj);
    }
    
    socket.on('logout',function(logout){
        console.log('logout 이벤트를 받았습니다.');
        console.dir(logout);
        
        console.log('로그아웃 : '+logout.id);
        
        delete login_ids[logout.id];
        delete socket.login_id;
        
        sendResponse(socket,'logout','200','로그아웃되었습니다.');
    });
    
    socket.on('room',function(room){
        console.log('room 이벤트를 받았습니다.');
        console.dir(room);
        
        if(room.command ==='create'){
            if(io.sockets.adapter.rooms[room.roomId]){
                console.log('방이 이미 만들어져 있습니다.');
            }else{
                console.log('방을 새로 만듭니다.');
                
                socket.join(room.roomId);
                
                var curRoom = io.sockets.adapter.rooms[room.roomId];
                curRoom.id = room.roomId;
                curRoom.name = room.roomName;
                curRoom.owner = room.roomOwner;
            }
        }else if(room.command ==='update'){
            var curRoom = io.sockets.adapter.rooms[room.roomId];
            curRoom.id = room.roomId;
            curRoom.name = room.roomName;
            curRoom.owner = room.roomOwner;
        }else if(room.command === 'delete'){
            socket.leave(room.roomId);
            
            if(io.sockets.adapter.rooms[room.roomId]){
                delete io.sockets.adapter.rooms[room.roomId];
            }else{
                console.log('방이 만들어져 있지 않습니다.');
            }
        }
        
        var roomList = getRoomList();
        
        var output = {command : 'list', rooms : roomList};
        console.log('클라이언트로 보낼 데이터 : '+JSON.stringify(output));
        
        io.sockets.emit('room',output);
        
    });
    
    function getRoomList(){
        
        console.log(io.sockets.adapter.rooms);
        
        var roomList= [];
        var index = 0;
        
        Object.keys(io.sockets.adapter.rooms).forEach(function(roomId){
            console.log('current room id : '+roomId);
            var outRoom = io.sockets.adapter.rooms[roomId];
            
            var foundDefault = false;
           
            Object.keys(outRoom.sockets).forEach(function(key){
                console.log('#'+index+' : '+key+', '+outRoom.sockets[key]);
                
                if(roomId == key){
                    foundDefault = true;
                    console.log('this is default room');
                }
                index++;
            });
            
            if(!foundDefault){
                roomList.push(outRoom);
            }
        });
        
        console.log('[ROOM LIST]');
        console.dir(roomList);
        
        return roomList;
    }
    
});