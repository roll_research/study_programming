class MyInt{
	int val;
	MyInt(int i){
		val=i;
	}
	
}


public class CallByValueObject {
	public static void main (String args[]){
		Person00 aPerson = new Person00("홍길동");
		MyInt a = new MyInt(33);
		
		aPerson.setAge(a);
		
		System.out.println(a.val);
	}
}
