package test07;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketClient {

	static final int PORT = 8888;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
			Socket socket = new Socket("localhost",PORT);
			System.out.println("Console> PORT("+PORT+") 로 접속합니다");
			
			InputStream stream = socket.getInputStream();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			
			String response = br.readLine();
			
			System.out.println("console> 수신 response: "+response);
			
			socket.close();
			
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

}
