﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            /* try
             {
                 while (true)
                 {
                     Console.WriteLine("가위 바위 보를 하겠습니다.\n" + "가위(0) 바위(1) 보(2)를 선택해주세요");
                     string select = Console.ReadLine();
                     int select_int = int.Parse(select);
                     Random robot = new Random();
                     int robot_select = robot.Next(0, 3);
                     if (select_int == 0)
                     {
                         switch (robot_select)
                         {
                             case 0: Console.WriteLine("본인 : 가위 상대: 가위를 냈습니다.\n 비겼습니다."); break;
                             case 1: Console.WriteLine("본인 : 가위 상대: 바위를 냈습니다.\n 졌습니다."); break;
                             case 2:
                                 Console.WriteLine("본인 : 가위 상대: 보를 냈습니다.\n  이겼습니다.\n 종료하겠습니다.");
                                 Environment.Exit(0); break;
                         }
                     }
                     else if (select_int == 1)
                     {
                         switch (robot_select)
                         {
                             case 0:
                                 Console.WriteLine("본인 : 바위 상대: 가위를 냈습니다.\n 이겼습니다.\n 종료하겠습니다.");
                                 Environment.Exit(0); break;
                             case 1: Console.WriteLine("본인 : 바위 상대: 바위를 냈습니다.\n 비겼습니다."); break;
                             case 2: Console.WriteLine("본인 : 바위 상대: 보를 냈습니다.\n  졌습니다."); break;
                         }
                     }
                     else if (select_int == 2)
                     {
                         switch (robot_select)
                         {
                             case 0: Console.WriteLine("본인 : 보 상대: 가위를 냈습니다.\n 졌습니다."); break;
                             case 1:
                                 Console.WriteLine("본인 : 보 상대: 바위를 냈습니다.\n 이겼습니다.\n 종료하겠습니다.");
                                 Environment.Exit(0); break;
                             case 2: Console.WriteLine("본인 : 보 상대: 보를 냈습니다.\n  비겼습니다."); break;
                         }
                     }
                 }
             }
             catch(FormatException ex)
             {
                 Console.WriteLine(ex.Data);
                 Console.WriteLine("오류입니다. 다시 시작해주시기 바랍니다.");
             } */

            //Console.WriteLine("Hello world");

            /*var name = "오" + "한" + "성";
            Console.WriteLine("Hello C# Programming");
            int sum = 10 + 20 + 30 + 2;
            int integer = 273;

            Console.WriteLine(name + sum + integer);*/

            /*Console.WriteLine(52 + 273);
            Console.WriteLine(5 + 3 * 2);
            Console.WriteLine(10 % 5);
            Console.WriteLine(7 % 3); */

            /*Console.WriteLine(1 + 2);
            Console.WriteLine(1 - 2);
            Console.WriteLine(1 * 2);
            Console.WriteLine(1 / 2);
            Console.WriteLine(1 % 2);
            */

            /*Console.WriteLine(52.273);
            Console.WriteLine(0);
            Console.WriteLine(0.0);
            */

            /*Console.WriteLine(1.0 + 2.0);
            Console.WriteLine(1.0 - 2.0);
            Console.WriteLine(1.0 * 2.0);
            Console.WriteLine(1.0 / 2.0);*/

            //Console.WriteLine(5.0 % 2.2); //실수에서는 % 연산을 사용하지 않는 것을 추천.

            /*Console.WriteLine('A');
            Console.WriteLine('가');
            Console.WriteLine('나');

            Console.WriteLine("안녕하세요.\n"); */

            //Console.WriteLine("가나다" + "라마" + "바사아" + "자아카타" + "파하");

            /*Console.WriteLine("안녕하세요"[0]);
            Console.WriteLine("안녕하세요"[1]);
            Console.WriteLine("안녕하세요"[3]);*/

            //Console.WriteLine("안녕하세요"[100]); //해당되는 배열의 번호방이 없기 때문에 작업을 하지 못한다.
            //Console.WriteLine('가' + '힣');

            //Console.WriteLine(52<273);
            //Console.WriteLine(52>273);

            /*Console.WriteLine(!true);
            Console.WriteLine(!false);
            Console.WriteLine(!(52 < 273));
            Console.WriteLine(!(52 > 273));*/

            /* int number = 10;
             Console.WriteLine(3 < number || number > 8);
             */

            /*Console.WriteLine(DateTime.Now.Hour < 3 || 8 < DateTime.Now.Hour);
            Console.WriteLine(3 < DateTime.Now.Hour && DateTime.Now.Hour < 8);*/

            /*int a = 273;
            int b = 52;

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            Console.WriteLine(a % b);3
            */

            /*int a = 2147483640;
            int b = 5227;

            Console.WriteLine(a + b);
            */

            /*int a = 2000000000;
            int b = 1000000000;

            Console.WriteLine(a + b);
            Console.WriteLine((long)a + b);
            Console.WriteLine(a + (long)b);
            Console.WriteLine((long)a + (long)b);
            */

            /* uint unsignedInt = 4147483647;
             ulong unsignedlong = 11223372036854775808;

             Console.WriteLine(unsignedInt);
             Console.WriteLine(unsignedlong);
             */

            /* Console.WriteLine(int.MaxValue);
             Console.WriteLine(int.MinValue);

             Console.WriteLine(long.MaxValue);
             Console.WriteLine(long.MinValue);
             */

            //Console.WriteLine(123456 + 65432L);

            /*double a = 52.273;
            double b = 103.32;

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            */

            /*char a = 'a';
            Console.WriteLine(a);*/

            /*Console.WriteLine("int :" + sizeof(int));
            Console.WriteLine("long :" + sizeof(long));
            Console.WriteLine("float :" + sizeof(float));
            Console.WriteLine("double :" + sizeof(double));
            Console.WriteLine("char :" + sizeof(char)); */

            /*char a = 'a';
            char b = 'b';

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            Console.WriteLine(a % b);
            */

            /*string message = "안녕하세요.";

            Console.WriteLine(message + "!");
            Console.WriteLine(message[0]);
            Console.WriteLine(message[1]);
            Console.WriteLine(message[2]);
            */

            // Console.WriteLine("string:" + sizeof(string)); //오류 메세지가 나온다.

            /*bool one = 10 < 0;
            bool other = 20 > 100;

            Console.WriteLine(one);
            Console.WriteLine(other); */

            /*string output = "hello ";
            output += "world";
            output += "!";

            Console.WriteLine(output);
            */

            /*int _int = 273;
            long _long = 522731;
            float _float = 52.273f;
            double _double = 52.273;
            char _char = '글';
            string _string = "문자열";

            Console.WriteLine(_int.GetType());
            Console.WriteLine(_float.GetType());
            Console.WriteLine(_long.GetType());
            Console.WriteLine(_double.GetType());
            Console.WriteLine(_char.GetType());
            Console.WriteLine(_string.GetType());
            */

            /*Console.WriteLine((273).GetType());
            Console.WriteLine((522731033L).GetType());
            Console.WriteLine((52.273F).GetType());
            Console.WriteLine((52.273).GetType());
            Console.WriteLine(('자').GetType());
            Console.WriteLine(("문자열").GetType());
            */

            /*var number = 100;
            Console.WriteLine(number.GetType());
            */

            /*string str=Console.ReadLine();
            Console.WriteLine("str: " + str);
            */

            //long longNumber = 214748367L + 2147483647L;
            //int intNumber = longNumber;
            //Console.WriteLine(intNumber); // 자료형이 큰 쪽(long)에서 작은 쪽 (int)로 넣어주려다보니 오류가 난다.

            /*long longNumber = 2147483647L + 2147483647L;
            int intNumber = (int)longNumber;
            Console.WriteLine(intNumber);*/

            /*int intNumber = 2147483647;

            long intToLong = intNumber;
            Console.WriteLine(intToLong);

            double intToDouble = intNumber;
            Console.WriteLine(intToDouble);
            */

            /*string numberString = "52273";
            int intNumber = (int)numberString; //오류, 실행 조차 되지 않는다.
            Console.WriteLine(intNumber);*/


            /*string numberString = "52273";
            int intNumber = int.Parse(numberString);
            Console.WriteLine(intNumber); */

            /*Console.WriteLine(int.Parse("52"));
            Console.WriteLine(long.Parse("273"));
            Console.WriteLine(float.Parse("52.273"));
            Console.WriteLine(double.Parse("103.32"));

            Console.WriteLine(int.Parse("52").GetType());
            Console.WriteLine(long.Parse("273").GetType());
            Console.WriteLine(float.Parse("52.273").GetType());
            Console.WriteLine(double.Parse("103.32").GetType());
            */

            /*Console.WriteLine((52).ToString());
            Console.WriteLine((52.273).ToString());
            Console.WriteLine(('a').ToString());
            Console.WriteLine((true).ToString());
            Console.WriteLine((false).ToString());

            Console.WriteLine((52).ToString().GetType());
            Console.WriteLine((52.273).ToString().GetType());
            Console.WriteLine(('a').ToString().GetType());
            Console.WriteLine((true).ToString().GetType());
            Console.WriteLine((false).ToString().GetType()); */

            /*double number = 52.273103;
            Console.WriteLine(number.ToString("0.0"));
            Console.WriteLine(number.ToString("0.00"));
            Console.WriteLine(number.ToString("0.000"));
            Console.WriteLine(number.ToString("0.0000"));
            */

            /*Console.WriteLine(52 + 273);
            Console.WriteLine("52" + 273);
            Console.WriteLine(52 + "273");
            Console.WriteLine("52" + "273");*/

            /*int number = 52273;
            string outputA = number + "";
            Console.WriteLine(outputA);

            char character = 'a';
            string outputB = character + "";
            Console.WriteLine(outputB);*/

            /*Console.WriteLine(bool.Parse("True"));
            Console.WriteLine(bool.Parse("False"));
            Console.WriteLine(bool.Parse("true"));
            Console.WriteLine(bool.Parse("false"));
            */

            /*int output = int.MinValue;
            Console.WriteLine(-output);
            */

            /*if (DateTime.Now.Hour < 12)
            {
                Console.WriteLine("오전입니다.");
            }

            else
            {
                Console.WriteLine("오후입니다.");
            }*/

            /*if (DateTime.Now.Hour < 11)
            {
                Console.WriteLine("아침 먹을 시간입니다.");
            }
            else if (DateTime.Now.Hour < 15)
                {
                    Console.WriteLine("점심 먹을 시간 입니다.");}
            else
                {
                    Console.WriteLine("저녁 먹을 시간 입니다.");
                }
                */

            /*Console.Write("이번 달은 몇 월인가요?");
            int input = int.Parse(Console.ReadLine());

            switch(input){
                case 12:
                case 1:
                case 2: Console.WriteLine("겨울입니다.");
                    break;
                case 3:
                case 4:
                case 5: Console.WriteLine("봄입니다.");
                    break;
                case 6:
                case 7:
                case 8: Console.WriteLine("여름입니다.");
                    break;
                case 9:
                case 10:
                case 11: Console.WriteLine("가을입니다.");
                    break;
                default:
                    Console.WriteLine("잘못되었습니다.");
                    break;
            }*/

            /*int number = 9;

            Console.WriteLine(number % 2 == 0 ? true : false);

            Console.WriteLine(number % 2 == 0 ? "짝수" : "홀수"); */

            /*Console.Write("입력:");
            string line = Console.ReadLine();

            if (line.Contains("안녕"))
            {
                Console.WriteLine("안녕하세요....!");
            }
            else
            {
                Console.WriteLine("^^");
            }*/
            /*
            ConsoleKeyInfo info = Console.ReadKey();
            switch (info.Key)
            {
                case ConsoleKey.UpArrow:
                    Console.WriteLine("위로 이동");
                    break;
                case ConsoleKey.DownArrow:
                    Console.WriteLine("아래로 이동");
                    break;
                case ConsoleKey.LeftArrow:
                    Console.WriteLine("왼쪽으로 이동");
                    break;
                case ConsoleKey.RightArrow:
                    Console.WriteLine("오른쪽으로 이동");
                    break;
            }*/

            /*string input = "Potato Tomato";
            Console.WriteLine(input.ToUpper());
            Console.WriteLine(input.ToLower());
            */

            /*string input = "감자 고구마 토마토";
            string[] inputs = input.Split(new char[] { ' ' });
            
            foreach(var item in inputs)
            {
                Console.WriteLine(item);
            }
            */

            /*string input = " test      \n";
            Console.WriteLine("::" + input.Trim() + "::");
            Console.Read();
            */

            /*string[] array = { "감자", "고구마", "토마토", "가지" };
            Console.WriteLine(string.Join(",", array));
            */

            /*Console.Write("메서드 호출 전");
            Console.SetCursorPosition(5, 5);
            Console.Write("메서드 호출 후");
            */

            /*Console.WriteLine("첫 번째 출력");
            Thread.Sleep(1000);
            Console.WriteLine("두 번째 출력");
            Thread.Sleep(1000);
            Console.WriteLine("세 번째 출력");
            */

            /*int x = 1;
            while (x < 50)
            {
                Console.Clear();
                Console.SetCursorPosition(x, 5);

                if(x%3 == 0)
                {
                    Console.WriteLine("__@");
                }else if (x %3 == 1)
                {
                    Console.WriteLine("_^@");
                }
                else
                {
                    Console.WriteLine("^_@");
                }
                Thread.Sleep(100);
                x++;
            }
            */

            bool state = true;


            while (state)
            {
                ConsoleKeyInfo info = Console.ReadKey();
                switch (info.Key)
                {
                    case ConsoleKey.UpArrow:
                        Console.WriteLine("위로 이동");
                        break;
                    case ConsoleKey.DownArrow:
                        Console.WriteLine("아래로 이동");
                        break;
                    case ConsoleKey.LeftArrow:
                        Console.WriteLine("좌로 이동");
                        break;
                    case ConsoleKey.RightArrow:
                        Console.WriteLine("우로 이동");
                        break;
                    case ConsoleKey.X:
                        state = false;
                        break;
                } 
            }
        }
    }
}
