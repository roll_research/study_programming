package com.rubypaper;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Service
public class LoggingRunner implements ApplicationRunner {

	private Logger logger = LoggerFactory.getLogger(LoggingRunner.class);
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		logger.trace("TRACE 로그 메세지");
		logger.debug("DEBUG 로그 메세지");
		logger.info("INFO 로그 메세지");
		logger.warn("WARN 로그 메세지");
		logger.error("ERROR 로그 메세지");
	}

	
	
}
