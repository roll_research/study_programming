﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_Player
{
    class Program
    {
        static void Main(string[] args)
        {
            Music_Insert Insert = new Music_Insert();
            Console.WriteLine("뮤직 플래이어 입니다.");
            int Controler = 0;

            while (Controler==0) {
                Console.WriteLine("해당되는 항목을 골라주세요!");
                int Number = 0;
                try
                {
                    Number = int.Parse(Console.ReadLine());
                }
                catch (Exception e)
                {
                    e.GetType();
                }
                switch (Number)
                {
                    case 1:
                        Console.WriteLine("노래 추가!");
                        Console.Write("이름: ");
                        string Name = Console.ReadLine();
                        Console.Write("항목: ");
                        string List = Console.ReadLine();
                        Console.Write("재생시간: ");
                        int Time = int.Parse(Console.ReadLine());
                        Insert.Music_Inserts(Name, List, Time);
                        break;
                    case 2:
                        Console.WriteLine("노래 삭제!");
                        Console.Write("이름: ");
                        Name = Console.ReadLine();
                        Insert.delete_Music(Name);
                        break;
                    case 3:
                        Console.WriteLine("노래 재생!");
                        Insert.print();
                        break;
                    case 4:
                        Console.WriteLine("노래 변경");
                        Console.WriteLine("노래를 변경할 이름을 적어주세요.");
                        string Before_Name = Console.ReadLine();
                        Console.WriteLine("새로 수정할 항목들을 적어주세요.");
                        Console.Write("이름: ");
                        string After_Name = Console.ReadLine();
                        Console.Write("항목: ");
                        List = Console.ReadLine();
                        Console.Write("재생시간: ");
                        Time = int.Parse(Console.ReadLine());
                        Insert.Adjust_Music(Before_Name,After_Name, List, Time);
                        break;
                    default:
                        Console.WriteLine("작동 에러. 종료합니다.");
                        Controler = 1;
                        break;
                }
            }
        }
    }
}
