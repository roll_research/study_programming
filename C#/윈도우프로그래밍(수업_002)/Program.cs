﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApplication4
{
    class Pyramid
    {
        int line;
        int mode;
        public void SetLine(int line) { this.line = line; }
        public void SetMode(int mode) { this.mode = mode; }
        public void Draw()
        {
            switch (mode)
            {
                case 1:
                    DrawLeft();
                    break;
                case 2:
                    DrawRight();
                    break;
                case 3:
                    DrawCenter();
                    break;
            }
        }
        private void DrawLeft()
        {
            for(int i = 0; i < line; i++)
            {
                for (int j = 0; j < i + 1; j++) Console.Write('*');
                Console.Write('\n');
            }
        }
        private void DrawRight()
        {
            Console.WriteLine("미완성");
        }
        private void DrawCenter()
        {
            Console.WriteLine("미완성");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Pyramid p = new Pyramid();
            int line;
            int mode;
            line = int.Parse(Console.ReadLine());
            mode = int.Parse(Console.ReadLine());
            
            p.SetLine(line);
            p.SetMode(mode);
            p.Draw();
            
        }
    }
}
