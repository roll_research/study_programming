﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(var item in Controls)
            {
                if(item is GroupBox)
                {
                    foreach(var internalitem in ((GroupBox)item).Controls)
                    {
                        RadioButton radio = internalitem as RadioButton;
                        if(radio!=null && radio.Checked)
                        {
                            MessageBox.Show(radio.Text);
                        }
                    }
                }
            }
        }
    }
}
