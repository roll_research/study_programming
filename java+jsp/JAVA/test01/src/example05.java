import java.util.Scanner;


public class example05 {
	public static void main (String [] args){
		
		int time;
		int second;
		int minute;
		int hour;
		Scanner sn = new Scanner(System.in);
		
		System.out.println("정수를 입력하세요");
		
		time = sn.nextInt();
		second= time % 60;
		minute = (time/60)%60;
		hour = (time/60)/60;
		
		System.out.println(time + "초는");
		System.out.println(hour + "시간");
		System.out.println(minute + "분");
		System.out.println(second + "초입니다.");
	}
}
