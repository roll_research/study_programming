﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication13
{
    //public delegate void TestDelegateA();
    class Program
    {
        public delegate void TestDelegate();
        public delegate void CustomDelegate();
        static void Main(string[] args)
        {
            TestDelegate delegateA=TestMethod;
            TestDelegate delegateB=delegate() { };
            TestDelegate delegateC=()=> { };

            delegateA();
            delegateB();
            delegateC();
        }
        static void TestMethod()
        {

        }
        public void Method(CustomDelegate customDelegate)
        {
            customDelegate();
        }
    }
}
