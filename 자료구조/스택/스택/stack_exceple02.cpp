#include <stdio.h>
#include <stdlib.h>

typedef int element;

typedef struct StackNode {
	struct StackNode *link;
	element item;
}StackNode;

typedef struct {
	StackNode *top;
}linkedStackNode;

void push(linkedStackNode *s, element item) {
	StackNode *temp = (StackNode*)malloc(sizeof(StackNode));
	if (temp == NULL) {
		fprintf(stderr, "잘못 만들어졌습니다.");
		return;
	}
	temp->item = item;
	temp->link = s->top;
	s->top = temp;

}

element pop(linkedStackNode *s) {
	if (is_empty(s)) {
		fprintf(stderr, "잘못 만들어졌습니다.");
		exit(1);
	}
	StackNode *temp = s->top;
	int item = temp->item;
	s->top = temp->link;
	free(temp);
	return item;
}