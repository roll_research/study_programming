# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 16:20:12 2020

@author: GM190524
"""

import pandas as pd
import seaborn as sns

titanic = sns.load_dataset('titanic')
df = titanic.loc[:,['age','sex','class','fare','survived']]

grouped = df.groupby(['class'])

std_all = grouped.std()
print(std_all)
print()
print(type(std_all))
print()

std_fare = grouped.fare.std()
print(std_fare)
print()
print(type(std_fare))

def min_max(x):
    return x.max() - x.min()

agg_minmax = grouped.agg(min_max)
print(agg_minmax.head())
print()

agg_all = grouped.agg(['min','max'])
print(agg_all.head())
print()

agg_sep = grouped.agg({'fare':['min','max'],'age':'mean'})
print(agg_sep.head())