﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    struct imformation{
        string Name;
        int age;
        int ki;

        public imformation(string name, int age, int ki)
        {
            this.Name = name;
            this.age = age;
            this.ki = ki;
        }

        public string Get_Name()
        {
            return this.Name;
        }

        public int Get_Age()
        {
            return this.age;
        }
        public int Get_Ki()
        {
            return this.ki;
        }
    }
    class Person
    {
        Queue<imformation> person_Queue = new Queue<imformation>();

        public void Enqueue(imformation im)
        {
            person_Queue.Enqueue(im);
            Console.WriteLine("입력완료");
        }

        public imformation Dequeue()
        {
            return person_Queue.Dequeue();
        }

        public imformation Peek()
        {
            return person_Queue.Peek();
        }

        public void Total_Customer()
        {
            imformation[] strQueue = new imformation[person_Queue.Count];
            person_Queue.CopyTo(strQueue,0);

            for (int i = 0; i < strQueue.Length; i++)
            {
                Console.WriteLine("이름: " + strQueue[i].Get_Name() + "나이: " + strQueue[i].Get_Age() + "키: " + strQueue[i].Get_Ki());
            }

        }
    }
}
