
public abstract class Animal {

	protected String name;
	protected String sexual;
	protected int age;	
	protected String bark;
	protected String wark;
	protected String eat;
	
	//이름, 성별, 나이, 짖는법, 걷는법, 먹는 것;
	
	
	public String getBark() {
		return bark;
	}
	public void setBark(String bark) {
		this.bark = bark;
	}
	public String getWark() {
		return wark;
	}
	public void setWark(String wark) {
		this.wark = wark;
	}
	public String getEat() {
		return eat;
	}
	public void setEat(String eat) {
		this.eat = eat;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSexual() {
		return sexual;
	}
	public void setSexual(String sexual) {
		this.sexual = sexual;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	public abstract void Bark(String bark);
	
	public abstract void warking(String wark);
	
	public abstract void eating(String eat);
	
	public abstract void information();
	
	
}
