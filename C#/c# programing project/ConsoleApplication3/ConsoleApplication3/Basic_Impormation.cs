﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Basic_Impormation
    {
        string Name;
        int How_many;
        int Price;
        string Order;

        public void ReSet_Store_Customer()
        {
            Name = null;
            How_many = 0;
            Price = 0;
            Order = null;
        }

        public void Set_Name(string Name)
        {
            this.Name = Name;
        }

        public string Get_Name()
        {
            return this.Name;
        }

        public void Set_How_many(int How_many)
        {
            this.How_many = How_many;
        }

        public int Get_How_many()
        {
            return this.How_many;
        }
        public void Set_Price(int Price)
        {
            this.Price = Price;
        }

        public int Get_Price()
        {
            return this.Price;
        }
        public void Set_Order(string Order)
        {
            this.Order = Order;
        }

        public string Get_Order()
        {
            return this.Order;
        }

    }
}
