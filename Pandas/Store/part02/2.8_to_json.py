# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 20:13:08 2020

@author: GM190524
"""

import pandas as pd

data = {'name': ['Jerry','Riah','Paul'],
        'algol':["A","A+","B"],
        'basic':["C","B","B+"],
        'c++':["B+","C","C+"],
        }

df = pd.DataFrame(data)
df.set_index('name',inplace=True)
print(df)

df.to_json("./df_sample.json")