# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 14:44:21 2020

@author: GM190524
"""

import seaborn as sns

df = sns.load_dataset('titanic')

nan_deck = df['deck'].value_counts(dropna=False)
print(nan_deck)

print(df.head().isnull())

print(df.head().notnull())

print(df.head().isnull().sum(axis=0))