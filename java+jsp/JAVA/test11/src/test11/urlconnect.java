package test11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class urlconnect {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
	try{	
		//URL url = new URL("http://localhost:8080/app/login_userServlet");
		
		//URL url = new URL("http://222.108.65.104:9900/app/login_userServlet"); 0
		
		//URL url = new URL("http://222.108.65.104:9900/app/show_microDust_on_regionServlet");0
		
		URL url = new URL("http://222.108.65.104:9900/app/change_actingMotionServlet");
		
		//URL url = new URL("http://222.108.65.104:9900/app/sign_upServlet"); 0
		
		//URL url = new URL("http://222.108.65.104:9900/app/show_indoor_microDust_on_regionServlet");
		
		//URL url = new URL("http://222.108.65.104:9900/app/show_outdoor_dust_region_findServlet");
		
		URLConnection conn = url.openConnection();
		HttpURLConnection hurlc = (HttpURLConnection)conn;
		
		hurlc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
		hurlc.setRequestMethod("POST");
		hurlc.setDoOutput(true);
		hurlc.setDoInput(true);
		hurlc.setUseCaches(false);
		hurlc.setDefaultUseCaches(false);
		
		//테스트
		//String param = "name="+"한성"+"&age="+"18";
		
		//로그인
		//String param = "user_id="+"abc1234"+"&user_password="+"123456789a";0
		
		//야외 미세먼지 정보 가져오기
		//String param = "aircleaner_id="+"1";0
		
		//수동 동작 기능 
		String param = "user_id="+"abc1234"+"&ON_OFF_checking="+"true"+"&manual_acting_checking="+"true";
		
		//회원가입 기능
		//String param = "user_id="+"abc1235"+"&user_password="+"123456789a"+"&user_name="+"한성"+"&region="+"충남 아산시"+"&user_phon_number="+"01012345678"+"&aircleaner_id="+"1"; 0
		
		//실내 미세먼지 정보 가져오기 기능
		//String param = "aircleaner_id="+"1";
		
		//지역에 따른 미세먼지 정보 가져오기
		//String param = "region="+"충남 아산시";
		
		
		OutputStream opstrm = hurlc.getOutputStream();
		opstrm.write(param.getBytes());
		opstrm.flush();
		opstrm.close();
		
		String buffer = null;
		String total_buffer = "";
		BufferedReader in = new BufferedReader(new InputStreamReader(hurlc.getInputStream()));
		while((buffer = in.readLine())!= null){
			total_buffer= total_buffer+buffer;
		}
		System.out.println(total_buffer);

	}finally{
		System.out.println("end");
	}
	}
}
