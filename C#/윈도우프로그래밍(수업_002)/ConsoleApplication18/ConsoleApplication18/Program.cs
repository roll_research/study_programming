﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication18
{
    delegate int SampleDelegate(int a, int b);
    delegate void PrintDelegate(string str);
    class Calulator { };
    class Print_Result
    {
        public void PrintName(string name) { Console.WriteLine("name = " + name); }
        public void PrintTitle(string title) { Console.WriteLine("title = " + title); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Print_Result pr = new Print_Result();
            PrintDelegate pd = null;
            pd =new PrintDelegate(pr.PrintName);
            pd("bksun");
        }
    }
}
