﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
    class Program
    {
        class Product
        {
            public string name;
            public int price;
            public static double PI = 3.141592;
        }


        static void Main(string[] args)
        {
            Product producta = new Product() { name = "고구마", price = 2000 };
            Product productb = new Product() { name = "감자", price = 3000 };
            Console.WriteLine(Product.PI); 
        }
    }
}
