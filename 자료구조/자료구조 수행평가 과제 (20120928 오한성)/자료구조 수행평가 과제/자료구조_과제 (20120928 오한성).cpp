#include <stdio.h>
#include <stdlib.h>
#define TRUE 1
#define FALSE 0

typedef int element;

struct tNode {
	int data;
	struct tNode *left, *right;
};

typedef struct Stack {
	tNode *item;
	struct Stack *link;
	tNode *top;
}Stack;

int is_empty(Stack *s) {
	return (s == NULL);
}

void push(Stack **s, tNode *item)
{
	Stack *temp = (Stack *)malloc(sizeof(Stack));
	if (s == NULL) {
		fprintf(stderr, "메모리 할당에러\n");
		return;
	}
	else {
		temp->item = item;
		temp->link = (*s);
		(*s) = temp;
	}
}

tNode *pop(Stack **s)
{
	if (is_empty(*s)) {
		fprintf(stderr, "스택이 비어있음\n");
		exit(1);
	}
	else {
		Stack *temp = (*s);
		tNode *item = temp->item;
		(*s) = (*s)->link;
		free(temp);
		return item;
	}
}

void inOrder(struct tNode *root) {
	struct tNode *current = root;
	struct Stack *s = NULL;
	bool done = 0;

	while (!done)
	{
		if (current != NULL)
		{
			push(&s, current);
			current = current->left;
		}
		else {
			if (!is_empty(s)) {
				current = pop(&s);
				printf("%d ", current->data);
				current = current->right;
			}
			else
				done = 1;
		}
	}
}

void postOrder(struct tNode *root) {
	struct tNode *current = root;
	struct Stack *s1 = NULL;
	struct Stack *s2 = NULL;
	bool done1 = 0;
	bool done2 = 0;

	push(&s1, current);

	while (!done1)
	{
		tNode *temp = pop(&s1);
		push(&s2, temp);
		if (temp->left != NULL) {
			push(&s1, temp->left);
		}
		if (temp->right != NULL) {
			push(&s1, temp->right);
		}
		if (is_empty(s1)) {
			done1 = 1;
		}
	}
	while (!done2)
	{
		current = pop(&s2);
		printf("%d ", current->data);
		if (is_empty(s2)) {
			done2 = 1;
		}
	}
}

void preOrder(struct tNode *root) {
	struct tNode *current = root;
	struct Stack *s = NULL;
	bool done = 0;

	while (!done)
	{
		if (current != NULL)
		{
			printf("%d ", current->data);
			push(&s, current);
			current = current->left;
		}
		else {
			if (!is_empty(s)) {
				current = pop(&s);
				current = current->right;
			}
			else
				done = 1;
		}
	}
}

void CreateNode(int node, tNode **t1) {
	if (!*t1) {
		*t1 = (tNode *)malloc(sizeof(tNode));
		(*t1)->data = node;
		(*t1)->left = NULL;
		(*t1)->right = NULL;
		return;
	}
	else if ((*t1)->left == NULL) {
		CreateNode(node, &(*t1)->left);
	}
	else if ((*t1)->right == NULL) {
		CreateNode(node, &(*t1)->right);
	}
	else {
		tNode *tmp = *t1;
		if (tmp->left->left == NULL || tmp->left->right == NULL) {
			tmp = tmp->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->left == NULL || tmp->right->right == NULL) {
			tmp = tmp->right;
			CreateNode(node, &tmp);
		}
		else if (tmp->left->left->left == NULL || tmp->left->left->right == NULL) {
			tmp = tmp->left->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->left->right->left == NULL || tmp->left->right->right == NULL) {
			tmp = tmp->left->right;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->left->left == NULL || tmp->right->left->right == NULL) {
			tmp = tmp->right->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->right->left == NULL || tmp->right->right->left == NULL) {
			tmp = tmp->right->right;
			CreateNode(node, &tmp);
		}
		else if (tmp->left->left->left->left == NULL || tmp->left->left->left->right == NULL) {
			tmp = tmp->left->left->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->left->left->right->left == NULL || tmp->left->left->right->right == NULL) {
			tmp = tmp->left->left->right;
			CreateNode(node, &tmp);
		}
		else if (tmp->left->right->left->left == NULL || tmp->left->right->left->right == NULL) {
			tmp = tmp->left->right->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->left->right->right->left == NULL || tmp->left->right->right->left == NULL) {
			tmp = tmp->left->right->right;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->left->left->left == NULL || tmp->right->left->left->right == NULL) {
			tmp = tmp->right->left->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->left->right->left == NULL || tmp->right->left->right->right == NULL) {
			tmp = tmp->right->left->right;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->right->left->left == NULL || tmp->right->right->left->right == NULL) {
			tmp = tmp->right->right->left;
			CreateNode(node, &tmp);
		}
		else if (tmp->right->right->right->left == NULL || tmp->right->right->right->right == NULL) {
			tmp = tmp->right->right->right;
			CreateNode(node, &tmp);
		}
	}
}



void main() {
	tNode *T1 = NULL;
	int insert;
	int num;
	printf("트리 노드의 개수를 입력하세요.");
	scanf("%d", &insert);
	printf("트리 노드를 입력하세요.");
	for (int i = 0; i < insert; i++) {
		scanf("%d", &num);
		CreateNode(num, &T1);
	}
	printf("전위 표기식\n");
	preOrder(T1);
	printf("\n");
	printf("중위 표기식\n");
	inOrder(T1);
	printf("\n");
	printf("후기 표기식\n");
	postOrder(T1);
	printf("\n");

}