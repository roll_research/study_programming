# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 17:25:54 2020

@author: GM190524
"""

import pandas as pd

url = './sample.html'

tables = pd.read_html(url)
print(len(tables))
print('\n')

for i in range(len(tables)):
    print("tables[%s]"% i)
    print(tables[i])
    print('\n')
    
df = tables[1]

df.set_index(['name'],inplace=True)
print(df)