# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 12:54:46 2020

@author: GM190524
"""

import pandas as pd
import numpy as np
from sklearn import preprocessing

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horsepower','weight',
              'acceleration','model year','origin','name']

df['horsepower'].replace('?',np.nan,inplace=True)
df.dropna(subset=['horsepower'],axis=0,inplace=True)
df['horsepower'] = df['horsepower'].astype('float')

count, bin_divider = np.histogram(df['horsepower'],bins=3)

bin_name = ['저출력','보통출력','고출력']

df['hp_bin'] = pd.cut(x=df['horsepower'],
                      bins=bin_divider,
                      labels=bin_name,
                      include_lowest=True)

horsepower_dummies = pd.get_dummies(df['hp_bin'])
print(horsepower_dummies.head(15))

label_encoder = preprocessing.LabelEncoder()
onehot_encoder = preprocessing.OneHotEncoder()

onehot_labeled = label_encoder.fit_transform(df['hp_bin'].head(15))
print(onehot_labeled)
print(type(onehot_labeled))

onehot_reshaped = onehot_labeled.reshape(len(onehot_labeled),1)
print(onehot_reshaped)
print(type(onehot_reshaped))

onehot_fitted = onehot_encoder.fit_transform(onehot_reshaped)
print(onehot_fitted)
print(type(onehot_fitted))









