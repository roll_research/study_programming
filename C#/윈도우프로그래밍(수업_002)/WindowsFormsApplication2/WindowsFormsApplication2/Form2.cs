﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            Label label = new Label()
            {
                Text = "글자",
            Location = new Point(10, 30),
            };
            LinkLabel linkLabel = new LinkLabel()
            {
                Text = "글자",
                Location = new Point(10, 70),
            };
            linkLabel.Click += LabelClick;

            Controls.Add(label);
            Controls.Add(linkLabel);
        }

        private void 열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("파일을 불려옵니다.");
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }
        private void LabelClick(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://hanb.co.kr");
            System.Diagnostics.Process.Start("notepad.exe");
        }
    }
}
