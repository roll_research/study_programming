﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(var item in Controls)
            {
                if(item is RadioButton)
                {
                    RadioButton radiobutten = (RadioButton)item;
                    if (radiobutten.Checked)
                    {
                        MessageBox.Show(radiobutten.Text);
                    }
                }
            }
        }
    }
}
