# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 15:40:00 2020

@author: GM190524
"""

import matplotlib.pyplot as plt
import seaborn as sns

titanic = sns.load_dataset('titanic')

sns.set_style('whitegrid')

g= sns.FacetGrid(data=titanic,col='who',row='survived')

g = g.map(plt.hist,'age')