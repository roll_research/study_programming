let target = {sports:"스포츠",music:"음악"};
let newProxy = new Proxy(target,{
    deleteProperty(target,key){
        return target[key];
    }
});
console.log(delete newProxy.sports);
console.log(delete newProxy.dummy);

Object.seal(target);
let desc = Object.getOwnPropertyNames(target,"music");
if(desc.configurable){
    console.log(delete newProxy.music);
}else{
    console.log("삭제 불가");
};