﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// 별 피라미드 만들기.
namespace ConsoleApplication11
{
    class Program
    {
        static void Main(string[] args)
        {

            for(int i=0; i<10; i++)
            {
                for (int j = 0; j < 10-i; j++)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < (i*2)+1; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
