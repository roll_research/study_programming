package programes;
import java.lang.Comparable;

public class Book implements Comparable<Book> {
	private String title;
	private String ISBN;
	private String author;
	private String company;
	private int price;
	public Book(){
		
	}
	
	public Book(String title,String ISBN, String author,String company,int price) {
		this.title = title;
		this.ISBN =ISBN;
		this.author=author;
		this.company=company;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public boolean equals(Book b){
		if(this.title.equals(b.title)&&this.ISBN.equals(b.ISBN)&&this.author.equals(b.author)&&this.company.equals(b.company)&&this.price==b.price){
			return true;
		}else{
			return false;
		}
	}
	public String toString(){
		return "이름:"+this.title + " ISBN:"+ this.ISBN + " 책 제목:" + this.author + " 회사:"+ this.company + " 가격:"+this.price;
	}
	

	@Override
	public int compareTo(Book b) {
		if(this.price<b.price){
			return -1;
		}else if (this.price >b.price){
			return 1;
		}else{
			return 0;
		}
	}
}

