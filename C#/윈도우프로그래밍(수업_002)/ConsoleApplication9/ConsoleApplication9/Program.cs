﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Test
    {
        public int Power(int x)
        {
        return x* x;
        }
    }
    class MyMath
    {
        public static int Abs (int input)
        {
            if (input < 0)
            {
                return -input;
            }
            else
            {
                return input;
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Test test = new Test();
            Console.WriteLine(test.Power(3));
            Console.WriteLine(test.Power(10));
            MyMath.Abs(5);
        }
    }
}
