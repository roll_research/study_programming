package com.springbook.biz;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;


public class TVUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		AbstractApplicationContext factory =  new GenericXmlApplicationContext("applicationContext.xml");
		
		TV tv = (TV) factory.getBean("tv");
		
		tv.powerOn();
		tv.volumUp();
		tv.volumDown();
		tv.powerOff();
		
		TV tv1 = (TV) factory.getBean("tv");
		TV tv2 = (TV) factory.getBean("tv");
		TV tv3 = (TV) factory.getBean("tv");
		
		factory.close();
		
		
		/*
		BeanFactory Factory = new BeanFactory();
		TV tv = (TV) Factory.getBean(args[0]);
		
		tv.powerOn();
		tv.volumUp();
		tv.volumDown();
		tv.powerOff();
		*/
		/*
		TV tv = new SamsungTV();
		
		tv.powerOn();
		tv.volumUp();
		tv.volumDown();
		tv.powerOff();
		*/
	}
}
