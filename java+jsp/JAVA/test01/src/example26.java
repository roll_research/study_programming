public class example26 {
	String name;
	int price;
	int numberOfstock;
	int sold;
	
	public static void main (String []args){
		example26 exam = new example26();
		
		exam.name = "Nikon";
		exam.price = 400000;
		exam.numberOfstock = 30;
		exam.sold = 50;
		
		System.out.println("상품 이름 :"+ exam.name);
		System.out.println("상품 가격 :"+ exam.price);
		System.out.println("재고 수량 :"+ exam.numberOfstock);
		System.out.println("팔린 수량 :"+ exam.sold);
	}
}
