const newMap = new Map([
    ["key1","value1"],
    [{},"오브젝트"]
]);

let sportObj = {};
newMap.set(sportObj,"추가");

console.log(newMap.delete("key1"));
console.log(newMap.delete({}));
console.log(newMap.delete(sportObj));