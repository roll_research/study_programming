# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 15:11:51 2020

@author: GM190524
"""

import seaborn as sns

df = sns.load_dataset('titanic')

print(df['embark_town'][825:830])
print()

most_freq = df['embark_town'].value_counts(dropna=True).idxmax()
print(most_freq)
print()

df['embark_town'].fillna(most_freq,inplace=True)
print(df['embark_town'][825:830])