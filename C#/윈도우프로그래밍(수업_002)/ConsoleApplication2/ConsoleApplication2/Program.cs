﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                int x = 0, y = 0, z = 0;
                bool state = true;
                while (state)
                {
                    ConsoleKeyInfo info = Console.ReadKey();
                    Console.SetCursorPosition(x,y);
                    switch (info.Key)
                    {
                        case ConsoleKey.UpArrow:
                            if (z % 3 == 0)
                            {
                                Console.WriteLine("__@");
                            }else if (z % 3 == 1)
                            {
                                Console.WriteLine("_^@");
                            }
                            else
                            {
                                Console.WriteLine("^_@");
                            }
                            ++z;
                            --y;
                            break;
                        case ConsoleKey.DownArrow:
                            if (z % 3 == 0)
                            {
                                Console.WriteLine("__@");
                            }
                            else if (z % 3 == 1)
                            {
                                Console.WriteLine("_^@");
                            }
                            else
                            {
                                Console.WriteLine("^_@");
                            }
                            ++z;
                            ++y;
                            break;
                        case ConsoleKey.LeftArrow:
                            if (z % 3 == 0)
                            {
                                Console.WriteLine("__@");
                            }
                            else if (z % 3 == 1)
                            {
                                Console.WriteLine("_^@");
                            }
                            else
                            {
                                Console.WriteLine("^_@");
                            }
                            ++z;
                            --x;
                            break;
                        case ConsoleKey.RightArrow:
                            if (z % 3 == 0)
                            {
                                Console.WriteLine("__@");
                            }
                            else if (z % 3 == 1)
                            {
                                Console.WriteLine("_^@");
                            }
                            else
                            {
                                Console.WriteLine("^_@");
                            }
                            ++z;
                            ++x;
                            break;
                        case ConsoleKey.X:
                            state = false;
                            break;
                    }
                    Thread.Sleep(200);
                    Console.Clear();
                }
            }catch(System.ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("더 이상 갈 수 있는 방향이 없습니다.");
            }
            }
    }
}
