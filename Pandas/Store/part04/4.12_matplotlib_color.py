# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 13:06:52 2020

@author: GM190524
"""

import matplotlib

colors={}

for name, hex in matplotlib.colors.cnames.items():
    colors[name] = hex

print(colors)