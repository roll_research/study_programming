# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 16:10:42 2020

@author: GM190524
"""

import pandas as pd

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horsepower','weight','acceleration','model year','origin','name']

df[['mpg','cylinders']].plot(kind='box')