class Sports{
    getGround(){
        return "상암 구장";
    }
};

let newSports = new Sports();

let newProxy = new Proxy(newSports,{
    getPrototypeOf(targets){
        return Object.getPrototypeOf(targets);
    }
});

console.log(Object.getPrototypeOf(newProxy));