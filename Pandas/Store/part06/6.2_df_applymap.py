# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 17:42:23 2020

@author: GM190524
"""

import seaborn as sns

titanic = sns.load_dataset('titanic')

df = titanic.loc[:,['age','fare']]
print(df.head())
print()

def add_10(n):
    return n + 10

df_map = df.applymap(add_10)
print(df_map.head())