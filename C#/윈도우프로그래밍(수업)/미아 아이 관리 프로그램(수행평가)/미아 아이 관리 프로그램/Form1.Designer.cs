﻿namespace 미아_아이_관리_프로그램
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.boy_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.girl_button = new System.Windows.Forms.Button();
            this.protected_person_button = new System.Windows.Forms.Button();
            this.select_button = new System.Windows.Forms.Button();
            this.name_box = new System.Windows.Forms.TextBox();
            this.age_box = new System.Windows.Forms.TextBox();
            this.self_phone_box = new System.Windows.Forms.TextBox();
            this.address_box = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.self_rescute_button = new System.Windows.Forms.Button();
            this.backup_button = new System.Windows.Forms.Button();
            this.result_impormation = new System.Windows.Forms.ListBox();
            this.current_impormation = new System.Windows.Forms.ListBox();
            this.save_butten = new System.Windows.Forms.Button();
            this.open_butten = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // boy_button
            // 
            this.boy_button.Location = new System.Drawing.Point(12, 288);
            this.boy_button.Name = "boy_button";
            this.boy_button.Size = new System.Drawing.Size(75, 77);
            this.boy_button.TabIndex = 0;
            this.boy_button.Text = "소년";
            this.boy_button.UseVisualStyleBackColor = true;
            this.boy_button.Click += new System.EventHandler(this.boy_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(567, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "미아 관리 프로그램";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "해당되는 정보를 입력하세요";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "이름";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "나이";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "전화번호";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "주소";
            // 
            // girl_button
            // 
            this.girl_button.Location = new System.Drawing.Point(93, 288);
            this.girl_button.Name = "girl_button";
            this.girl_button.Size = new System.Drawing.Size(75, 77);
            this.girl_button.TabIndex = 7;
            this.girl_button.Text = "소녀";
            this.girl_button.UseVisualStyleBackColor = true;
            this.girl_button.Click += new System.EventHandler(this.girl_button_Click);
            // 
            // protected_person_button
            // 
            this.protected_person_button.Location = new System.Drawing.Point(174, 288);
            this.protected_person_button.Name = "protected_person_button";
            this.protected_person_button.Size = new System.Drawing.Size(75, 77);
            this.protected_person_button.TabIndex = 8;
            this.protected_person_button.Text = "장애아이";
            this.protected_person_button.UseVisualStyleBackColor = true;
            this.protected_person_button.Click += new System.EventHandler(this.protected_person_button_Click);
            // 
            // select_button
            // 
            this.select_button.Location = new System.Drawing.Point(14, 371);
            this.select_button.Name = "select_button";
            this.select_button.Size = new System.Drawing.Size(235, 143);
            this.select_button.TabIndex = 9;
            this.select_button.Text = "실시간 정보 삭제";
            this.select_button.UseVisualStyleBackColor = true;
            this.select_button.Click += new System.EventHandler(this.select_button_Click);
            // 
            // name_box
            // 
            this.name_box.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.name_box.Location = new System.Drawing.Point(69, 124);
            this.name_box.Name = "name_box";
            this.name_box.Size = new System.Drawing.Size(177, 21);
            this.name_box.TabIndex = 10;
            // 
            // age_box
            // 
            this.age_box.Location = new System.Drawing.Point(69, 164);
            this.age_box.Name = "age_box";
            this.age_box.Size = new System.Drawing.Size(177, 21);
            this.age_box.TabIndex = 11;
            // 
            // self_phone_box
            // 
            this.self_phone_box.Location = new System.Drawing.Point(71, 204);
            this.self_phone_box.Name = "self_phone_box";
            this.self_phone_box.Size = new System.Drawing.Size(177, 21);
            this.self_phone_box.TabIndex = 12;
            // 
            // address_box
            // 
            this.address_box.Location = new System.Drawing.Point(69, 248);
            this.address_box.Name = "address_box";
            this.address_box.Size = new System.Drawing.Size(177, 21);
            this.address_box.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(253, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "실시간 정보";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(787, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "실종 아이 정보";
            // 
            // self_rescute_button
            // 
            this.self_rescute_button.Location = new System.Drawing.Point(823, 541);
            this.self_rescute_button.Name = "self_rescute_button";
            this.self_rescute_button.Size = new System.Drawing.Size(206, 23);
            this.self_rescute_button.TabIndex = 16;
            this.self_rescute_button.Text = "해당 미아 삭제";
            this.self_rescute_button.UseVisualStyleBackColor = true;
            this.self_rescute_button.Click += new System.EventHandler(this.self_rescute_button_Click);
            // 
            // backup_button
            // 
            this.backup_button.Location = new System.Drawing.Point(1035, 541);
            this.backup_button.Name = "backup_button";
            this.backup_button.Size = new System.Drawing.Size(195, 23);
            this.backup_button.TabIndex = 17;
            this.backup_button.Text = "전체 기록 삭제";
            this.backup_button.UseVisualStyleBackColor = true;
            this.backup_button.Click += new System.EventHandler(this.backup_button_Click);
            // 
            // result_impormation
            // 
            this.result_impormation.FormattingEnabled = true;
            this.result_impormation.ItemHeight = 12;
            this.result_impormation.Location = new System.Drawing.Point(255, 114);
            this.result_impormation.Name = "result_impormation";
            this.result_impormation.Size = new System.Drawing.Size(528, 400);
            this.result_impormation.TabIndex = 18;
            // 
            // current_impormation
            // 
            this.current_impormation.FormattingEnabled = true;
            this.current_impormation.ItemHeight = 12;
            this.current_impormation.Location = new System.Drawing.Point(789, 114);
            this.current_impormation.Name = "current_impormation";
            this.current_impormation.Size = new System.Drawing.Size(441, 400);
            this.current_impormation.TabIndex = 19;
            // 
            // save_butten
            // 
            this.save_butten.Location = new System.Drawing.Point(14, 520);
            this.save_butten.Name = "save_butten";
            this.save_butten.Size = new System.Drawing.Size(113, 44);
            this.save_butten.TabIndex = 20;
            this.save_butten.Text = "실종 정보 저장";
            this.save_butten.UseVisualStyleBackColor = true;
            this.save_butten.Click += new System.EventHandler(this.button1_Click);
            // 
            // open_butten
            // 
            this.open_butten.Location = new System.Drawing.Point(133, 520);
            this.open_butten.Name = "open_butten";
            this.open_butten.Size = new System.Drawing.Size(113, 44);
            this.open_butten.TabIndex = 21;
            this.open_butten.Text = "실종 정보 부름";
            this.open_butten.UseVisualStyleBackColor = true;
            this.open_butten.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "텍스트 파일|*.txt";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "텍스트 파일|*.txt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 576);
            this.Controls.Add(this.open_butten);
            this.Controls.Add(this.save_butten);
            this.Controls.Add(this.current_impormation);
            this.Controls.Add(this.result_impormation);
            this.Controls.Add(this.backup_button);
            this.Controls.Add(this.self_rescute_button);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.address_box);
            this.Controls.Add(this.self_phone_box);
            this.Controls.Add(this.age_box);
            this.Controls.Add(this.name_box);
            this.Controls.Add(this.select_button);
            this.Controls.Add(this.protected_person_button);
            this.Controls.Add(this.girl_button);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.boy_button);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button boy_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button girl_button;
        private System.Windows.Forms.Button protected_person_button;
        private System.Windows.Forms.Button select_button;
        private System.Windows.Forms.TextBox name_box;
        private System.Windows.Forms.TextBox age_box;
        private System.Windows.Forms.TextBox self_phone_box;
        private System.Windows.Forms.TextBox address_box;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button self_rescute_button;
        private System.Windows.Forms.Button backup_button;
        private System.Windows.Forms.ListBox result_impormation;
        private System.Windows.Forms.ListBox current_impormation;
        private System.Windows.Forms.Button save_butten;
        private System.Windows.Forms.Button open_butten;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

