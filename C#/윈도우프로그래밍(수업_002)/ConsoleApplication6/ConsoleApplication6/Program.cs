﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            /*bool state = true;
            int i = 0;
            while (state) {
                Console.WriteLine("출력");
                i++;
                if (i == 5)
                {
                    state = false;
                }
            }*/
            /*
            int[] intarray = { 10, 20, 30 };
            long[] longArray = { 52, 273, 32, 65, 103 };
            float[] floatArray = { 1.0F, 2.0F, 3.0F, 4.0F };
            */

            /*
            int[] intarray = { 52, 273, 32, 65, 103 };
            intarray[0] = 0;

            Console.WriteLine(intarray[0]);
            Console.WriteLine(intarray[1]);
            Console.WriteLine(intarray[2]);
            Console.WriteLine(intarray[3]);
            Console.WriteLine(intarray.Length);
            */
            /*
            int[] array = new int[100];

            Console.WriteLine(array[0]);
            Console.WriteLine(array[99]);
            */
            /*
            int i = 0;
            int[] intArray = { 52, 273, 32, 65, 103 };

            while (i < intArray.Length)
            {
                Console.WriteLine(i + "번째 출력:" + intArray[i]);
                i++;
            }
            */
            /*
            string input;

            do
            {
                Console.WriteLine("입력(exit을 입력하면 종료):");
                input = Console.ReadLine();
            } while (input != "exit");
            */
            /*
            int output = 0;

            for(int i = 0; i <= 100; i++)
            {
                output += i;
            }
            Console.WriteLine(output);
            */
            /*
            int output = 1;

            for(int i=1; i <= 20; i++)
            {
                output *= i;
            }
            Console.WriteLine(output);
            */

            /*
            long start = DateTime.Now.Ticks;
            long count = 0;

            while (start + (1000000) > DateTime.Now.Ticks)
            {
                count++;
            }
            Console.WriteLine(count + "만큼 반복했습니다.");
            */
            /*
            int[] intArray = { 1, 2, 3, 4, 5 };
            for(int i =intArray.Length-1; i >= 0; i--)
            {
                Console.WriteLine(intArray[i]);
            }
            */
            /*
            string[] Array = { "사과", "배", "포도", "딸기", "바나나" };

            foreach(var item in Array)
            {
                Console.WriteLine(item);
            }
            */
            /*
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < i+1; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10-i; j++)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < i+1;j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            */
            /*
            while (true)
            {
                Console.Write("숫자를 입력하세요(짝수를 입력하면 종료):");
                int input = int.Parse(Console.ReadLine());
                if(input % 2 == 0)
                {
                    break;
                }
            }
            */
            /*
            for(int i =1;i<10; i++)
            {
                if (i % 2 == 0)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
            */

            /*
            string input = "Potato Tomato";
            Console.WriteLine(input.ToUpper());
            Console.WriteLine(input.ToLower());
            */
            /*
            string input = "Potato Tomato";
            input.ToUpper();
            Console.WriteLine(input);
            */
            /*
            string addtion = "안녕 반가워! 게이들의 세상은 처음이지?";
            string [] inputs=addtion.Split(new char[] {' '});
            foreach(var item in inputs)
            {
                Console.WriteLine(item);
            }
            */
            /*
            string input = "\t  test    \n";
            Console.WriteLine(":: " + input.Trim() + "::");
            Console.Read();
            */
            /*
            string[] array = { "감자", "고구마", "토마토", "가지" };
            Console.WriteLine(string.Join(",", array));
            */

            int[] array = new int[20];
            int[] array2 = new int[3] { 1, 2, 3 };
            int[] array3 = new int[] { 1, 2, 3 };
            int[] array4 = { 1, 2, 3 };
        }
    }
}
