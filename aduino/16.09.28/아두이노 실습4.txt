#include <LiquidCrystal.h>

LiquidCrystal lcd(40, 54, 41, 42, 43, 44, 45);

const int cdsInPin = A0;
const int analogInPin = A1;

const int ledPin1 =24;
const int ledPin2 =25;
const int ledPin3 =26;
const int ledPin4 =27;

int sensorValue;  // 조도센서 아날로그 입력핀 설정
int cdsValue;     // 포텐쇼 ㅁ[타 아날로그 입력핀 설정

void setup() {
  pinMode(ledPin1,OUTPUT);
  pinMode(ledPin2,OUTPUT);
  pinMode(ledPin3,OUTPUT);
  pinMode(ledPin4,OUTPUT);
  Serial.begin(9600);
  
  lcd.begin(8,2);
  lcd.clear();  
}

void loop() {
  sensorValue = analogRead(analogInPin);
  cdsValue = analogRead(cdsInPin);

  Serial.print("Sensor = " );
  Serial.print(sensorValue);
  Serial.print(" ");
  Serial.print("cds = " );
  Serial.println(cdsValue);

  digitalWrite(ledPin1,HIGH);
  digitalWrite(ledPin2,HIGH);
  digitalWrite(ledPin3,HIGH);
  digitalWrite(ledPin4,HIGH);
  
 if(cdsValue>=20){
  lcd.clear();  
  lcd.setCursor(0,0);
  lcd.print("OFF");
  lcd.setCursor(0,1);
  lcd.print(cdsValue);
   digitalWrite(ledPin1,HIGH);
  digitalWrite(ledPin2,HIGH);
  digitalWrite(ledPin3,HIGH);
  digitalWrite(ledPin4,HIGH);
 }else{
  lcd.clear();  
  lcd.setCursor(0,0);
  lcd.print("ON");
  lcd.setCursor(0,1);
  lcd.print(cdsValue);
  digitalWrite(ledPin1,LOW);
  digitalWrite(ledPin2,LOW);
  digitalWrite(ledPin3,LOW);
  digitalWrite(ledPin4,LOW);
 }    
  delay(500);
  
}