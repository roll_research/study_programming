var express = require('express')
, http = require('http')
, path = require('path');

var bodyParser = require('body-parser')
,cookieParser = require('cookie-parser')
,static = require('serve-static')
,errorHandler = require('errorhandler');

var expressErrorHandler = require('express-error-handler');
var expressSession = require('express-error-handler');

var mongoose = require('mongoose');
var crypto = require('crypto');

var user = require('./routes/user');
var config = require('./config');


var app = express();

console.log('config server_port : %d',config.server_port);
app.set('port',process.env.PORT||config.server_port);

app.use(bodyParser.urlencoded({ extended: false}));

app.use(bodyParser.json());

app.use('/public',static(path.join(__dirname,'public')));

app.use(cookieParser());

app.use(expressSession({
    secret:'my key',
    resave:true,
    saveUninitialized:true
}));

var MongoClient = require('mongodb').MongoClient;

var database;

var UserSchema;

var UserModel;

function connectDB(){
    
    var databaseUrl = 'mongodb://localhost:27017/local';
    
    console.log('데이터베이스 연결을 시도합니다.');
    mongoose.Promise = global.Promise;
    mongoose.connect(databaseUrl,{useUnifiedTopology: true, useNewUrlParser: true});
    database = mongoose.connection;
    
    database.on('error',console.error.bind(console,'mongoose connection error.'));
    database.on('open',function(){
        console.log('데이터베이스에 연결되었습니다. :'+databaseUrl);
        
        createUserSchema();
        
        });
    
    database.on('disconnected',function(){
        console.log('연결이 끊어졌습니다. 5초 후 다시 연결합니다.');
        setInterval(connectDB,5000);
    })
}

function createUserSchema(){
    
    UserSchema = require('./database/user_schema').createSchema(mongoose);
    
    UserModel = mongoose.model("users3",UserSchema);
    console.log('UserModel 정의함');
    
    user.init(database,UserSchema,UserModel);
    
}

var router = express.Router();

router.route('/process/login').post(user.login);

router.route('/process/adduser').post(user.adduser);

router.route('/process/listuser').post(user.listuser);

user.init(app);

app.use('/',router);

var errorHandler = expressErrorHandler({
    static:{
        '404':'./public/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

http.createServer(app).listen(app.get('port'),function(){
    console.log('서버가 시작되었습니다. 포트 :'+ app.get('port'));
    
    connectDB();
});