# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 16:27:48 2020

@author: GM190524
"""

import folium

seoul_map2 = folium.Map(location=[37.55,126.98],tiles='Stamen Terrain',
                        zoom_start=12)

seoul_map3 = folium.Map(location=[37.55,126.98],tiles='Stamen Toner',
                        zoom_start=12)

seoul_map2.save('./seoul2.html')
seoul_map3.save('./seoul3.html')