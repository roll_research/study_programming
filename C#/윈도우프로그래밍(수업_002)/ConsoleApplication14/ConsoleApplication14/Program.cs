﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication14
{
    class Student
    {
        public string Name { get; set; }
        public double Score { get; set; }

        public Student (string name, double score)
        {
            this.Name = name;
            this.Score = score;
        }

        public override string ToString()
        {
            return this.Name + " : " + this.Score;
        }
    }
    class Students
    {
        private List<Student> listOfStudent = new List<Student>();

        public delegate void PrintProcess(string list);

        public void Add (Student student)
        {
            listOfStudent.Add(student);
        }
        public void Print()
        {
            Print((Student) =>
            {
                Console.WriteLine(Student);
            });
        }
        public void Print(PrintProcess process)
        {
            foreach (var item in listOfStudent)
            {
                process(item);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Students students = new Students();
            students.Add(new Student("윤인성", 4.2));
            students.Add(new Student("연하진", 4.4));

            students.Print();
            students.Print((Student) =>
            {
                Console.WriteLine();
                Console.WriteLine("이름: " + Student.name);
                Console.WriteLine("학점: " + Student.score);
            });
        }
    }
}
