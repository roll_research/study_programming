#include <Wire.h>

void setup() {
  // put your setup code here, to run once:
  
  Wire.begin();
  delay(500); 
}

void loop() {
  // put your main code here, to run repeatedly
 const int lookup[4] = {0x73,0x38,0x77,0x6E};

 int Count, Thousands, Hundreds, Tens, Base;  
 Wire.beginTransmission(0x38);
 Wire.write((byte)0);
 Wire.write(B01000111);
 Wire.endTransmission();

 Wire.beginTransmission(0x38);
 Wire.write(1);
 Base = 3;
 Tens = 2;
 Hundreds = 1;
 Thousands = 0;

 Wire.write(lookup[Thousands]);
 Wire.write(lookup[Hundreds]);
 Wire.write(lookup[Tens]);
 Wire.write(lookup[Base]);

 Wire.endTransmission();
 delay(100);
}

