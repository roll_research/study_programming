# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 17:47:01 2020

@author: GM190524
"""

import seaborn as sns

titanic = sns.load_dataset('titanic')
df= titanic.loc[:,['age','fare']]
print(df.head())
print()

def missing_values(series):
    return series.isnull()

result = df.apply(missing_values, axis=0)
print(result.head())
print(type(result))