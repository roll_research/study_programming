import java.util.Scanner;

public class example10 {
	public static void main (String [] args){
		char grade;
		
		Scanner sn = new Scanner (System.in);
		
		while(sn.hasNext()){
			int score = sn.nextInt();
			
			if(score >= 90.0)
				grade = 'A';
			else if (score >= 80.0)
				grade = 'B';
			else if (score >= 70.0)
				grade = 'C';
			else if (score >= 60.0)
				grade = 'D';
			else 
				grade = 'F';
			
			System.out.println("학점은 "+ grade + "입니다.");
		}
	}
}
