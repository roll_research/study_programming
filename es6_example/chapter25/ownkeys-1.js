let sportObj = Object.defineProperties({},{
    baseball:{value:"축구",enumerable:true},
    swim:{value:"수영"}
});

let newProxy = new Proxy(sportObj,{
    ownKeys(target){
        return Object.getOwnPropertyNames(target);
    }
});

console.log(Object.getOwnPropertyNames(newProxy));
console.log(Object.keys(newProxy));