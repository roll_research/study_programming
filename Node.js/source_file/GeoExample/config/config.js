



module.exports = {
    server_port : 3000,
    db_url : 'mongodb://localhost:27017/shopping',
    db_schemas : [
        {file: './user_schema',collection:'users6',
        schemaName: 'UserSchema',modelName:'UserModel'},
        {file: './coffeeshop_schema',collection:'coffeeshop',
        schemaName: 'CoffeeShopSchema',modelName:'CoffeeShopModel'}
    ],
    route_info:[
        {file:'./coffeeshop',path:'/process/addcoffeeshop',method:'add',type:'post'},
        {file:'./coffeeshop',path:'/process/listcoffeeshop',method:'list',type:'post'},
        {file:'./coffeeshop',path:'/process/nearcoffeeshop',method:'findNear',type:'post'},
     {file:'./coffeeshop',path:'/process/withincoffeeshop',method:'findWithin',type:'post'},
    {file:'./coffeeshop',path:'/process/circlecoffeeshop',method:'findCircle',type:'post'}
    ],
    facebook: {
        clientID : '610556363116190',
        clientSecret :'3b76269154ca0aa5ff16c96b41a6609f',
        callbackURL : '/auth/facebook/callback'
    }
    
}
