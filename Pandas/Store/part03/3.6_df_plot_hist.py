# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 16:03:27 2020

@author: GM190524
"""

import pandas as pd

df = pd.read_excel('./남북한발전전력량.xlsx')

df_ns = df.iloc[[0,5],3:]
df_ns.index = ['South','North']
df_ns.columns =df_ns.columns.map(int)
print(df_ns.head())
print()
df_ns.plot()

tdf_ns =df_ns.transpose()
print(tdf_ns.head())
print()
tdf_ns.plot(kind='hist')