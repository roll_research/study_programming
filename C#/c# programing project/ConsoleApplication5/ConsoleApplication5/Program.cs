﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        class PointClass
        {
            public int x;
            public int y;

            public PointClass(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        struct PointStruct
        {
            public int x;
            public int y;

            public PointStruct (int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }
        static void Main(string[] args)
        {
            PointClass PointClassA = new PointClass(10, 20);
            PointClass PointClassB = PointClassA;

            PointClassB.x = 100;
            PointClassB.y = 100;

            Console.WriteLine(PointClassA.x + "  " + PointClassA.y);
            Console.WriteLine(PointClassB.x + "  " + PointClassB.y);
            Console.WriteLine();

            PointStruct PointStructA = new PointStruct(10, 20);
            PointStruct PointStructB = PointStructA;

            PointStructB.x = 100;
            PointStructB.y = 100;

            Console.WriteLine(PointStructA.x + "  " + PointStructA.y);
            Console.WriteLine(PointStructB.x + "  " + PointStructB.y);
            
        }
    }
}
