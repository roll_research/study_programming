# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 16:47:06 2020

@author: GM190524
"""

import pandas as pd
import seaborn as sns

titanic = sns.load_dataset('titanic')
df = titanic.loc[:,['age','sex','class','fare','survived']]

grouped = df.groupby(['class'])

grouped_filter = grouped.filter(lambda x : len(x) >=200)
print(grouped_filter.head())
print()
print(type(grouped_filter))

age_filter = grouped.filter(lambda x : x.age.mean() < 30)
print(age_filter.tail())
print()
print(type(age_filter))