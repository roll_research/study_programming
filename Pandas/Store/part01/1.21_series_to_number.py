# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 13:21:08 2020

@author: GM190524
"""

import pandas as pd

student1 = pd.Series({'국어':100,'영어':80,'수학':90})
print(student1)
print('\n')

percentage = student1/200
print(percentage)
print('\n')
print(type(percentage))