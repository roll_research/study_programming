﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace 미아_아이_관리_프로그램
{
    public partial class Form1 : Form
    {
        string result_boy;
        string result_girl;
        string result_protected_person;
        int age;
        public Form1()
        {
            InitializeComponent();
        }

        private void boy_button_Click(object sender, EventArgs e)
        {
            boy b=new boy(name_box.Text, age_box.Text, self_phone_box.Text, address_box.Text);
            
            try
            {
                if (name_box.Text.Equals("") || age_box.Text.Equals("") || self_phone_box.Text.Equals("") || address_box.Text.Equals(""))
                {
                    MessageBox.Show("빈칸이 있습니다. 다시 채워주세요","알림");
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else if (int.Parse(age_box.Text) > 18)
                {
                    MyException me = new MyException();
                    me.select_exception_age();
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }else if (self_phone_box.Text.Length > 12)
                {
                    MyException me = new MyException();
                    me.select_exception_phone_number();
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else {
                    result_boy = "이름: " + name_box.Text + " 나이: " + age_box.Text + " 핸드폰 번호: " + self_phone_box.Text + " 주소: " + address_box.Text + "\n";
                    result_impormation.Items.Add(result_boy);
                    result_impormation.Items.Add(b.action());

                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";

                    current_impormation.Items.Add(result_boy.Trim() + " 소년");
                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show("나이란에 숫자를 적어주세요.","알림");
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
            }
        }
    

        private void girl_button_Click(object sender, EventArgs e)
        {
            girl g = new girl(name_box.Text, age_box.Text, self_phone_box.Text, address_box.Text);

            try
            {
                if (name_box.Text.Equals("") || age_box.Text.Equals("") || self_phone_box.Text.Equals("") || address_box.Text.Equals(""))
                {
                    MessageBox.Show("빈칸이 있습니다. 다시 채워주세요","알림");
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else if (int.Parse(age_box.Text) > 18)
                {
                    MyException me = new MyException();
                    me.select_exception_age();
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else if (self_phone_box.Text.Length > 12)
                {
                    MyException me = new MyException();
                    me.select_exception_phone_number();
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else {
                    result_girl = "이름: " + name_box.Text + " 나이: " + age_box.Text + " 핸드폰 번호: " + self_phone_box.Text + " 주소: " + address_box.Text + "\n";
                    result_impormation.Items.Add(result_girl);
                    result_impormation.Items.Add(g.action());

                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";

                    current_impormation.Items.Add(result_girl.Trim() + " 소녀");
                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show("나이란에 숫자를 적어주세요.","알림");
                name_box.Text = "";
                age_box.Text = "";
                self_phone_box.Text = "";
                address_box.Text = "";
            }
        }

        private void protected_person_button_Click(object sender, EventArgs e)
        {
            protected_person p = new protected_person(name_box.Text, age_box.Text, self_phone_box.Text, address_box.Text);

            try
            {
                if (name_box.Text.Equals("") || age_box.Text.Equals("") || self_phone_box.Text.Equals("") || address_box.Text.Equals(""))
                {
                    MessageBox.Show("빈칸이 있습니다. 다시 채워주세요","알림");
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else if (int.Parse(age_box.Text) > 18)
                {
                    MyException me = new MyException();
                    me.select_exception_age();
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else if (self_phone_box.Text.Length > 12)
                {
                    MyException me = new MyException();
                    me.select_exception_phone_number();
                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";
                }
                else {
                    result_protected_person = "이름: " + name_box.Text + " 나이: " + age_box.Text + " 핸드폰 번호: " + self_phone_box.Text + " 주소: " + address_box.Text + "\n";
                    result_impormation.Items.Add(result_protected_person);
                    result_impormation.Items.Add(p.action());

                    name_box.Text = "";
                    age_box.Text = "";
                    self_phone_box.Text = "";
                    address_box.Text = "";

                    current_impormation.Items.Add(result_protected_person.Trim() + " 장애아이");
                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show("나이란에 숫자를 적어주세요.","알림");
                name_box.Text = "";
                age_box.Text = "";
                self_phone_box.Text = "";
                address_box.Text = "";
            }

        }




        private void backup_button_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("실종 아이들의 기록 정보를 모두 삭제하시겠습니까?", "주의상황", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            try
            {
                if (dr == DialogResult.Yes)
                {
                    MessageBox.Show("정상적으로 삭제 되었습니다.", "알림");
                    current_impormation.Items.Clear(); 
                }
                else if (dr == DialogResult.No)
                {
                    MessageBox.Show("삭제를 하지 않습니다.", "알림");
                }
                else
                {
                    MessageBox.Show("원 장치로 돌아가겠습니다.", "알림");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("아무것도 입력된 것이 없습니다.", "알림");
            }
        }

        private void self_rescute_button_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("해당 실종 아이의 기록 정보를 삭제하시겠습니까?", "주의상황", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            try
            {
                if (dr == DialogResult.Yes)
                {
                    MessageBox.Show("정상적으로 삭제 되었습니다.", "알림");
                    current_impormation.Items.Remove(current_impormation.SelectedItem);
                }
                else if (dr == DialogResult.No)
                {
                    MessageBox.Show("삭제를 하지 않습니다.", "알림");
                }
                else
                {
                    MessageBox.Show("원 장치로 돌아가겠습니다.", "알림");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("아무것도 입력된 것이 없습니다.", "알림");
            }
        }

        private void select_button_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("실종 아이들의 실시간 정보를 모두 삭제하시겠습니까?", "주의상황", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            try
            {
                if (dr == DialogResult.Yes)
                {
                    MessageBox.Show("정상적으로 삭제 되었습니다.", "알림");
                    result_impormation.Items.Clear();
                }
                else if (dr == DialogResult.No)
                {
                    MessageBox.Show("삭제를 하지 않습니다.", "알림");
                }
                else
                {
                    MessageBox.Show("원 장치로 돌아가겠습니다.", "알림");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("아무것도 입력된 것이 없습니다.", "알림");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog()==DialogResult.OK)
            {
               
               int search = saveFileDialog1.FileName.LastIndexOf("\\");
               this.Text = saveFileDialog1.FileName.Substring(search + 1);
               StreamWriter sw = new StreamWriter(saveFileDialog1.FileName,false,Encoding.Default);
               foreach (var item in current_impormation.Items)
                {
                    sw.WriteLine(item.ToString());
                }
               
               sw.Close();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string openSrc = openFileDialog1.FileName;
                int search = openFileDialog1.FileName.LastIndexOf("\\");
                this.Text = openSrc.Substring(search + 1);

                current_impormation.Items.Clear();
                StreamReader sr = new StreamReader(openSrc, Encoding.Default);
                
                current_impormation.Items.AddRange(sr.ReadToEnd().Split('\n'));
                sr.Close();    
            }
        }
    }
}
