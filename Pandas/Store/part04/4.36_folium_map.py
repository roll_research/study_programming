# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 16:23:12 2020

@author: GM190524
"""

import folium

seoul_map = folium.Map(location=[37.55,126.98],zoom_start=12)

seoul_map.save('./seoul.html')