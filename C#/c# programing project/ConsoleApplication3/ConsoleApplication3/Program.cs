﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool Open = true;
            Store_Customer Customer = new Store_Customer();
            Console.WriteLine("가게를 오픈합니다.");
            while (Open)
            {
                Console.WriteLine("가게 사람들이 취할 행동을 선택하세요.(숫자 선택)");
                Console.WriteLine("1.손님 입장 ; 2. 손님 나감 ; 3.손님의 정보 ; 4. 가게 마감 ");
                int Number = 0;
                try
                {
                    Number = int.Parse(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("잘못된 정보를 넣으셨네요.");
                }
                switch (Number)
                {
                    case 1:
                        string Name = null;
                        int How_many = 0;
                        int Price = 0;
                        string Manu = null;

                        Console.WriteLine("손님 입장!! 정보 입력.");
                        try
                        {
                            Console.Write("이름: ");
                            Name = Console.ReadLine();
                            Console.Write("인원 수: ");
                            How_many = int.Parse(Console.ReadLine());
                            Console.Write("가격: ");
                            Price = int.Parse(Console.ReadLine());
                            Console.Write("매뉴: ");
                            Manu = Console.ReadLine();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("정보를 잘못 입력하셨습니다.");
                        }
                        Customer.Insert_Customer(Name,How_many,Price,Manu);
                        break;
                    case 2:
                        Console.WriteLine("손님 퇴장!!");
                        Customer.Delete_Customer();
                        break;
                    case 3:
                        Console.WriteLine("전체 손님 정보!!");
                        Customer.Total_Customer();
                        break;
                    case 4:
                        Console.WriteLine("장사 마감 합니다!!!");
                        Customer.Finish();
                        Open = false;
                        break;
                    default:
                        Console.WriteLine("선택 잘못하셨습니다. 다시 시작하세요!!");
                        Open = false;
                        break;
                }
                }
                }
    }
}
