package test06;

import java.text.SimpleDateFormat;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;


public class environment_api {

	air_info[] air_object = new air_info[50];
	
	public void Insert_environment_info(String nation){
			
		try{
				String ServiceKey = null;
				String urlstr = null;
			
				if(nation.equals("충남")){
				//String nation = "충남";
				ServiceKey = "dIKjWIBLjlM%2BuZXUspeyUu2s479dUgsIVws1DDUg5OY82UF3HvvfYTZBYPDvELyGyK0FU8lBWWXMZxtx%2BKiM9A%3D%3D";
				urlstr = "http://openapi.airkorea.or.kr/"
						+ "openapi/services/rest/ArpltnInforInqireSvc/"
						+ "getCtprvnMesureSidoLIst?sidoName="+nation+"&searchCondition=HOUR"
						+ "&pageNo=1&numOfRows=31&ServiceKey="+ServiceKey+"&_returnType=json";
				}
				else if (nation.equals("서울")){
				//String nation = "서울";
				ServiceKey = "dIKjWIBLjlM%2BuZXUspeyUu2s479dUgsIVws1DDUg5OY82UF3HvvfYTZBYPDvELyGyK0FU8lBWWXMZxtx%2BKiM9A%3D%3D";
				urlstr = "http://openapi.airkorea.or.kr/"
						+ "openapi/services/rest/ArpltnInforInqireSvc/"
						+ "getCtprvnMesureSidoLIst?sidoName="+nation+"&searchCondition=HOUR"
						+ "&pageNo=1&numOfRows=25&ServiceKey="+ServiceKey+"&_returnType=json";
				}
				else if (nation.equals("인천")){
				//String nation = "인천";
				ServiceKey = "dIKjWIBLjlM%2BuZXUspeyUu2s479dUgsIVws1DDUg5OY82UF3HvvfYTZBYPDvELyGyK0FU8lBWWXMZxtx%2BKiM9A%3D%3D";
				urlstr = "http://openapi.airkorea.or.kr/"
						+ "openapi/services/rest/ArpltnInforInqireSvc/"
						+ "getCtprvnMesureSidoLIst?sidoName="+nation+"&searchCondition=HOUR"
						+ "&pageNo=1&numOfRows=9&ServiceKey="+ServiceKey+"&_returnType=json";
				}
				
				URL url = new URL(urlstr);
				BufferedReader bf;
				String result = "";
				String line ="";
				bf = new BufferedReader(new InputStreamReader(url.openStream()));

				while((line = bf.readLine())!= null){
					result = result.concat(line);
				}
					
				 	JSONParser parser = new JSONParser();
			        JSONObject obj = (JSONObject) parser.parse(result);
			        JSONArray array = (JSONArray) obj.get("list");
			        
			        for(int i=0; i<array.size();i++){
			        	JSONObject object = (JSONObject)array.get(i);
			        	air_object[i]=new air_info(); 
			        	air_object[i].setCityname(nation+" "+object.get("cityName").toString());
			        	air_object[i].setDatatime(object.get("dataTime").toString());
			        	if(!object.get("pm10Value").toString().isEmpty()){
			        		air_object[i].setPm10Value(object.get("pm10Value").toString());
			        	}else{
			        		air_object[i].setPm10Value("-1");	
			        	}
			        	if(!object.get("pm25Value").toString().isEmpty()){
			        		air_object[i].setPm25Value(object.get("pm25Value").toString());
			        	}else{
			        		air_object[i].setPm25Value("-1");
			        	}
			        	System.out.println("도시:"+air_object[i].getCityname());
			        	System.out.println("시간:"+air_object[i].getDatatime());
			        	System.out.println("미세먼지 농도:"+air_object[i].getPm10Value());
			        	System.out.println("초시미세먼지 농도:"+air_object[i].getPm25Value());
			        	
			        	air_object[i].getting_number();
			        	
			        	DB database = new DB();
			        	database.INSERT_DB(air_object[i].getCityname(),air_object[i].getDatatime()
			        	,air_object[i].getPm10Value(),air_object[i].getPm25Value(),air_info.Number);
			        	//air_info.Number++;
			        	
			        	air_object[i].setting_number(air_info.Number);
			        }
			           
				bf.close();
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
	}
	
	
	
}
