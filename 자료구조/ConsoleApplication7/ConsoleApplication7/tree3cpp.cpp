#include <stdio.h>
#include <stdlib.h>

typedef struct node {

	int		data;
	struct node * lefe_link;
	struct node * right_link;
}Tree;
void Tree_ins(Tree **root, int data) {
	Tree *tmp = *root;
	if (*root == NULL) {
		*root = (Tree *)malloc(sizeof(Tree));
		(*root)->data = data;
		(*root)->lefe_link = NULL;
		(*root)->right_link = NULL;
	}
	else if (*root != NULL) {
		while (tmp&& tmp->data >data) {
			tmp = tmp->lefe_link;
		}
		while (tmp&& tmp->data<data) {
			tmp = tmp->right_link;
		}
		if (tmp->data >data) {
			tmp->lefe_link = (Tree *)malloc(sizeof(Tree));
			tmp->lefe_link->data = data;
			tmp->lefe_link->lefe_link = NULL;
			tmp->lefe_link->right_link = NULL;
		}
		else if (tmp->data <data) {
			tmp->right_link = (Tree *)malloc(sizeof(Tree));
			tmp->right_link->data = data;
			tmp->right_link->right_link = NULL;
			tmp->right_link->lefe_link = NULL;
		}
	}
}

void main() {
	int test[20] = { 45, 27, 17, 62, 57 , 73, 52, 65, 76, 69, 63, 64, 71 };
	Tree *root = NULL;
	for (int i = 0; test[i]>0; i++)
		Tree_ins(&root, test[i]);
}