﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
    delegate int SampleDelegate(int a, int b);

    class Calculator
    {
        public static int Sum(int a, int b) { return a + b; }
        public static int Sub(int a, int b) { return a - b; }
        public static int Multi(int a, int b) { return a * b; }
        public static int Div(int a, int b) { return a / b; }
    }


    class Program
    {
        static void Main(string[] args)
        {
            SampleDelegate calc = null;
            calc = new SampleDelegate(Calculator.Sum);
            Console.WriteLine("10+20: " + calc(10, 20));
            calc = new SampleDelegate(Calculator.Sub);
            Console.WriteLine("10-20: " + calc(10, 20));
        }
    }
}
