#include <stdio.h>
#include <stdlib.h>
#define Max 100

typedef int element;

typedef struct{
	element stack[Max];
	int top;
}StackType;

void init(StackType *s) {
	 s->top = -1;
}

int is_empty(StackType *s) {
	return (s->top == -1);
}

int is_full(StackType *s) {
	return (s->top == Max - 1);
}

void push(StackType *s, int data) {
	if (is_full(s)) {
		fprintf(stderr, "시스템 과부화 에러");
		return;
	}
	s->stack[++(s->top)] = data;
}

element pop(StackType *s) {
	if (is_empty(s)) {
		fprintf(stderr, "시스템 부족 에러");
		exit(1);
	}
	return s->stack[(s->top)--];
}

element peek(StackType *s) {
	if (is_empty(s)) {
		fprintf(stderr, "시스템 부족 에러");
		exit(1);
	}
	return s->stack[(s->top)];
}

