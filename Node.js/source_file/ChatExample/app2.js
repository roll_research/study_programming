var express = require('express')
, http = require('http')
, path = require('path');

var bodyParser = require('body-parser')
,cookieParser = require('cookie-parser')
,session = require('express-session')
,static = require('serve-static')
,errorHandler = require('errorhandler');

var expressErrorHandler = require('express-error-handler');
var expressSession = require('express-error-handler');

var mongoose = require('mongoose');

var crypto = require('crypto');

var database_loader = require('./database/database_loader');
var route_loader = require('./routes/route_loader');
var config = require('./config/config');

var socketio = require('socket.io');

var cors = require('cors');

var app = express();

app.set('port',process.env.PORT||3000);

app.use(bodyParser.urlencoded({ extended: false}));

app.use(bodyParser.json());

app.use('/public',static(path.join(__dirname,'public')));

app.use(cookieParser());

app.use(expressSession({
    secret:'my key',
    resave:true,
    saveUninitialized:true
}));

app.use(session({
    cookie :{maxAge:6000},
    secret : 'my key',
    resave : false,
    saveUninitialized:false
}));

app.set('views',__dirname+'/views');

app.set('view engine','ejs');

console.log('뷰 엔진이 ejs로 설정되었습니다.');

var passport = require('passport');
var flash = require('connect-flash');

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

var configPassport = require('./config/passport');
configPassport(app,passport);

app.use(cors());

var router = express.Router();
route_loader.init(app,router);

var userPassport = require('./routes/user_passport');
userPassport(router,passport);

var errorHandler = expressErrorHandler({
    static:{
        '404':'./public/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

var server = http.createServer(app).listen(app.get('port'),function(){
    console.log('서버가 시작되었습니다. 포트 :'+ app.get('port'));
    
    database_loader.init(app,config);
});

var io = socketio.listen(server);
console.log('socket.io 요청을 받아들일 준비가 되었습니다.');

io.sockets.on('connection',function(socket){
    console.log('connection info : ', socket.request.connection._peername);
    socket.remoteAddress = socket.request.connection._peername.address;
    socket.remotePort = socket.request.connection._peername.port;
    
    socket.on('message',function(message){
        console.log('message 이벤트를 받았습니다.');
        console.dir(message);
        
        if(message.recepient =='ALL'){
            console.log('나를 포함한 모든 클라이언트에게 message 이벤트를 전송합니다.');
            io.sockets.emit('message',message);
        }
    });
    
});