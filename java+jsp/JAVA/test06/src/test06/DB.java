package test06;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DB {

	 Connection conn = null;
     Statement stmt = null;
     ResultSet rs = null;
     //SimpleDateFormat format1 = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
     
     
     public void SELECT_DB () {
    	 try{
    		 Class.forName("com.mysql.jdbc.Driver"); 
    		 // 테스트용
    		 //String url = "jdbc:mysql://localhost/Test_Server";
             //conn = DriverManager.getConnection(url, "root", "dmschd5884!@");
    		 
    		 //라우터 안에 있을 경우
    		 //String url = "jdbc:mysql://192.168.0.6:3306/final_project?characterEncoding=UTF-8&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false";
             //conn = DriverManager.getConnection(url, "root", "rusy5884!@runner");
             
             //외부에서 접속할 경우
             String url = "jdbc:mysql://222.108.65.104:3306/final_project?characterEncoding=UTF-8&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false";
             conn = DriverManager.getConnection(url, "root", "rusy5884!@runner");
             
             stmt = conn.createStatement();
             String sql = "SELECT * FROM outdoor_info";
             rs = stmt.executeQuery(sql);
             while(rs.next()){
            	 int outdoor_info_number = rs.getInt(1);
            	 String outdoor_day_time = rs.getString(2);
            	 float outdoor_dust_info = rs.getFloat(3);
            	 float outdoor_micro_dust_info = rs.getFloat(4);
            	 String region = rs.getString(5);

                 System.out.println("외부 정보 넘버:"+outdoor_info_number + " 외부 정보 날짜 및 시간:" + outdoor_day_time +
                 " 외부 정보 미세먼지 정보:" + outdoor_dust_info + " 외부 정보 초미세먼지 정보:"+ outdoor_micro_dust_info+
                 " 외부 정보 지역:"+region);
               
             }
             
    		 
    	 }catch( ClassNotFoundException e){
             System.out.println("드라이버 로딩 실패");
         }
         catch( SQLException e){
             System.out.println("에러 " + e);
         }
         finally{
             try{
                 if( conn != null && !conn.isClosed()){
                     conn.close();
                 }
                 if( stmt != null && !stmt.isClosed()){
                     stmt.close();
                 }
                 if( rs != null && !rs.isClosed()){
                     rs.close();
                 }
             }
             catch( SQLException e){
                 e.printStackTrace();
             }
         }
     }
     
     
     public void INSERT_DB(String cityName, String dateTime, String pm10Value, String pm25Value, int number) throws ParseException{
    	  	 
    	 Connection conn = null;
         PreparedStatement pstmt = null;
    
         //Date to_DateTime = format1.parse(dateTime);
         //java.sql.Date s = new java.sql.Date(to_DateTime.getTime());
         //java.sql.Timestamp sql_Timestamp = new java.sql.Timestamp(to_DateTime.getTime());
         
         	try{
         		//테스트용
         		//String url = "jdbc:mysql://localhost:3306/test_server?characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false";
                //conn = DriverManager.getConnection(url, "root", "dmschd5884!@");
                
         		//같은 라우터에 있을경우
         		//String url = "jdbc:mysql://192.168.0.6:3306/final_project?characterEncoding=UTF-8&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false";
                //conn = DriverManager.getConnection(url, "root", "rusy5884!@runner");
                
                //외부에서 접속할 경우
                String url = "jdbc:mysql://222.108.65.104:3306/final_project?characterEncoding=UTF-8&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false";
                conn = DriverManager.getConnection(url, "root", "rusy5884!@runner");
                
                
                String sql = "INSERT INTO outdoor_info VALUES (?,?,?,?,?)";
                pstmt = conn.prepareStatement(sql);
                
                pstmt.setInt(1, number);
                pstmt.setString(2, dateTime);
                pstmt.setFloat(3, Float.parseFloat(pm10Value));
                pstmt.setFloat(4, Float.parseFloat(pm25Value));
                pstmt.setString(5, cityName);
                
                int count = pstmt.executeUpdate();
                
                if( count == 0 ){
                    System.out.println("데이터 입력 실패");
                }
                else{
                    System.out.println("데이터 입력 성공");
                }
                
         	} catch( SQLException e){
                System.out.println("에러 " + e);
            }
            finally{
                try{
                    if( conn != null && !conn.isClosed()){
                        conn.close();
                    }
                    if( pstmt != null && !pstmt.isClosed()){
                        pstmt.close();
                    }
                }
                catch( SQLException e){
                    e.printStackTrace();
                }
            }
        	 
     }
}
	
