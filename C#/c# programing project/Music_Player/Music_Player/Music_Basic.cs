﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_Player
{
    class Music_Basic
    {
        int Num = 0;
        int counter = 0;
        string Name;
        string List;
        int Time;
        List<string> Music_Name = new List<string>();
        List<string> Music_List = new List<string>();
        List<int> Music_Time = new List<int>();

        public void Music_Basic_Formet()
        {
            Name = null;
            List = null;
            Time = 0;
        }

        public void Set_Name(string Name)
        {
            this.Name = Name;
        }

        public string Get_Name()
        {
            return Name;
        }

        public void Set_List(string List)
        {
            this.List = List;
        }
        public string Get_List()
        {
            return List;
        }
        public void Set_Time(int Time)
        {
            this.Time = Time;
        }
        public int Get_Time()
        {
            return Time;
        }

        public void Insert_Music(string Name, string List, int Time)
        {
            this.Music_Name.Add(Name);
            this.Music_List.Add(List);
            this.Music_Time.Add(Time);
            counter++;
        }

        public void delete_Music(string name)
        {
            for (int i = 0; i < Music_Name.Count; i++)
            {
                if (Music_Name[i].Equals(name))
                {
                    this.Music_Name.RemoveAt(i);
                    this.Music_List.RemoveAt(i);
                    this.Music_Time.RemoveAt(i);
                    Console.WriteLine("삭제 되었습니다.");
                    break;
                }
            }
        }

       public void Adjust_Music (string Before_Name,string After_Name , string List, int Time)
        {
            for (int i = 0; i < Music_Name.Count; i++)
            {
                if (Music_Name[i].Equals(Before_Name))
                {
                    this.Music_Name.RemoveAt(i);
                    this.Music_List.RemoveAt(i);
                    this.Music_Time.RemoveAt(i);

                    this.Music_Name.Insert(i, After_Name);
                    this.Music_List.Insert(i, List);
                    this.Music_Time.Insert(i, Time);
                    Console.WriteLine("변경되었습니다.");
                    break;
                }
            }
        }

        public void print()
        {
            for(int i=0; i< Music_Name.Count; i++)
            {
                Console.WriteLine("제목: " + Music_Name[i] + " 분류: " + Music_List[i] + " 저장된 시간: " + Music_Time[i]);

            }
        }

    }
}
