# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 12:37:44 2020

@author: GM190524
"""

import pandas as pd
import matplotlib.pyplot as plt

plt.style.use('classic')

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horespower','weight',
              'acceleration','model year','origin','name']

cylinders_size = df.cylinders/df.cylinders.max() * 300

df.plot(kind='scatter',x='weight',y='mpg',c='coral',figsize=(10,5),
        s=cylinders_size,alpha=0.3)
plt.title('Scatter Plot - mpg-weight-cylinders')
plt.show()
