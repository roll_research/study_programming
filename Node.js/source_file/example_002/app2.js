var express = require('express')
, http = require('http')
, path = require('path');

var bodyParser = require('body-parser')
,cookieParser = require('cookie-parser')
,static = require('serve-static')
,errorHandler = require('errorhandler');

var expressErrorHandler = require('express-error-handler');
var expressSession = require('express-error-handler');

var crypto = require('crypto');

var user = require('./routes/user')

var config = require('./config');

var database_loader = require('./database/database_loader');
var route_loader = require('./routes/route_loader');


var app = express();

app.set('port',config.server_port || 3000);

app.use(bodyParser.urlencoded({ extended: false}));

app.use(bodyParser.json());

app.use('/public',static(path.join(__dirname,'public')));

app.use(cookieParser());

app.use(expressSession({
    secret:'my key',
    resave:true,
    saveUninitialized:true
})); 


route_loader.init(app,express.Router());


var errorHandler = expressErrorHandler({
    static:{
        '404':'./public/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

http.createServer(app).listen(app.get('port'),function(){
    console.log('서버가 시작되었습니다. 포트 :'+ app.get('port'));
    
    database_loader.init(app,config);
}); 