﻿namespace 로또_당첨기
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.address_box = new System.Windows.Forms.TextBox();
            this.boy = new System.Windows.Forms.Button();
            this.play_zoo = new System.Windows.Forms.Label();
            this.result_information = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.name_box = new System.Windows.Forms.TextBox();
            this.self_phone_box = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.age_box = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.girl = new System.Windows.Forms.Button();
            this.protected_person = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.beckup_button = new System.Windows.Forms.Button();
            this.current_situration = new System.Windows.Forms.ListBox();
            this.report = new System.Windows.Forms.Label();
            this.select = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // address_box
            // 
            this.address_box.Location = new System.Drawing.Point(84, 201);
            this.address_box.Name = "address_box";
            this.address_box.Size = new System.Drawing.Size(178, 21);
            this.address_box.TabIndex = 0;
            // 
            // boy
            // 
            this.boy.Location = new System.Drawing.Point(5, 277);
            this.boy.Name = "boy";
            this.boy.Size = new System.Drawing.Size(86, 23);
            this.boy.TabIndex = 1;
            this.boy.Text = "소년";
            this.boy.UseVisualStyleBackColor = true;
            this.boy.Click += new System.EventHandler(this.boy_Click);
            // 
            // play_zoo
            // 
            this.play_zoo.AutoSize = true;
            this.play_zoo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.play_zoo.Location = new System.Drawing.Point(536, 29);
            this.play_zoo.Name = "play_zoo";
            this.play_zoo.Size = new System.Drawing.Size(177, 12);
            this.play_zoo.TabIndex = 2;
            this.play_zoo.Text = "놀이공원 입장관 미아 관리 센터";
            this.play_zoo.Click += new System.EventHandler(this.rotto_butten_Click);
            // 
            // result_information
            // 
            this.result_information.FormattingEnabled = true;
            this.result_information.ItemHeight = 12;
            this.result_information.Location = new System.Drawing.Point(302, 79);
            this.result_information.Name = "result_information";
            this.result_information.Size = new System.Drawing.Size(657, 364);
            this.result_information.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 12);
            this.label1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(3, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "해당되는 버튼을 클릭하시기 바랍니다.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "전화번호";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "이름";
            // 
            // name_box
            // 
            this.name_box.Location = new System.Drawing.Point(84, 90);
            this.name_box.Name = "name_box";
            this.name_box.Size = new System.Drawing.Size(178, 21);
            this.name_box.TabIndex = 10;
            // 
            // self_phone_box
            // 
            this.self_phone_box.Location = new System.Drawing.Point(84, 164);
            this.self_phone_box.Name = "self_phone_box";
            this.self_phone_box.Size = new System.Drawing.Size(178, 21);
            this.self_phone_box.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "주소";
            // 
            // age_box
            // 
            this.age_box.Location = new System.Drawing.Point(84, 128);
            this.age_box.Name = "age_box";
            this.age_box.Size = new System.Drawing.Size(178, 21);
            this.age_box.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "나이";
            // 
            // girl
            // 
            this.girl.Location = new System.Drawing.Point(97, 277);
            this.girl.Name = "girl";
            this.girl.Size = new System.Drawing.Size(81, 23);
            this.girl.TabIndex = 15;
            this.girl.Text = "소녀";
            this.girl.UseVisualStyleBackColor = true;
            this.girl.Click += new System.EventHandler(this.girl_Click);
            // 
            // protected_person
            // 
            this.protected_person.AutoSize = true;
            this.protected_person.Location = new System.Drawing.Point(184, 277);
            this.protected_person.Name = "protected_person";
            this.protected_person.Size = new System.Drawing.Size(88, 23);
            this.protected_person.TabIndex = 16;
            this.protected_person.Text = "장애아동";
            this.protected_person.UseVisualStyleBackColor = true;
            this.protected_person.Click += new System.EventHandler(this.protected_person_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(41, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(221, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "해당 아이의 신상정보를 입력 해주세요!!";
            // 
            // beckup_button
            // 
            this.beckup_button.Location = new System.Drawing.Point(5, 331);
            this.beckup_button.Name = "beckup_button";
            this.beckup_button.Size = new System.Drawing.Size(267, 122);
            this.beckup_button.TabIndex = 18;
            this.beckup_button.Text = "화면 정리";
            this.beckup_button.UseVisualStyleBackColor = true;
            this.beckup_button.Click += new System.EventHandler(this.beckup_button_Click);
            // 
            // current_situration
            // 
            this.current_situration.FormattingEnabled = true;
            this.current_situration.ItemHeight = 12;
            this.current_situration.Location = new System.Drawing.Point(965, 79);
            this.current_situration.Name = "current_situration";
            this.current_situration.Size = new System.Drawing.Size(316, 364);
            this.current_situration.TabIndex = 19;
            // 
            // report
            // 
            this.report.AutoSize = true;
            this.report.Location = new System.Drawing.Point(963, 55);
            this.report.Name = "report";
            this.report.Size = new System.Drawing.Size(109, 12);
            this.report.TabIndex = 20;
            this.report.Text = "현재 실종된 아이들";
            // 
            // select
            // 
            this.select.Location = new System.Drawing.Point(1078, 44);
            this.select.Name = "select";
            this.select.Size = new System.Drawing.Size(203, 23);
            this.select.TabIndex = 21;
            this.select.Text = "확인하기";
            this.select.UseVisualStyleBackColor = true;
            this.select.Click += new System.EventHandler(this.select_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1293, 487);
            this.Controls.Add(this.select);
            this.Controls.Add(this.report);
            this.Controls.Add(this.current_situration);
            this.Controls.Add(this.beckup_button);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.protected_person);
            this.Controls.Add(this.girl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.age_box);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.self_phone_box);
            this.Controls.Add(this.name_box);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.result_information);
            this.Controls.Add(this.play_zoo);
            this.Controls.Add(this.boy);
            this.Controls.Add(this.address_box);
            this.Name = "Form1";
            this.Text = "경력";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox address_box;
        private System.Windows.Forms.Button boy;
        private System.Windows.Forms.Label play_zoo;
        private System.Windows.Forms.ListBox result_information;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox name_box;
        private System.Windows.Forms.TextBox self_phone_box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox age_box;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button girl;
        private System.Windows.Forms.Button protected_person;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button beckup_button;
        private System.Windows.Forms.ListBox current_situration;
        private System.Windows.Forms.Label report;
        private System.Windows.Forms.Button select;
    }
}

