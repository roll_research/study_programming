# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 13:58:55 2020

@author: GM190524
"""

import pandas as pd
import seaborn as sns

titanic = sns.load_dataset('titanic')
df = titanic.loc[:,['age','fare']]
print(df.head())
print()
print(type(df))
print()

addtion = df + 10
print(addtion.head())
print()
print(type(addtion))