# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 16:31:50 2020

@author: GM190524
"""

import pandas as pd

df = pd.DataFrame({'C1':['a','a','b','a','b'],
                   'C2':[1,1,1,2,2],
                   'C3':[1,1,2,2,2]})

print(df)
print()

df_dup = df.duplicated()
print(df_dup)
print()

col_dup = df['C2'].duplicated()
print(col_dup)