
var handler_loader = {};

var handler_info = require('./handler_info');
var utils = require('jayson/lib/utils');


handler_loader.init = function(jayson,app,api_path){
    console.log('handler_loader.init 호출됨');
    return initHandler(jayson,app,api_path);
};


function initHandler(jayson,app,api_path){
    var handlers = {};
    
    var infoLen = handler_info.length;
    console.log('설정에 정의된 핸드러의 수 : %d',infoLen);
    
    for(var i=0; i<infoLen;i++){
        var curItem = handler_info[i];
        
        var curHandler = require(curItem.file);
        console.log('%s 파일에서 모듈정보를 읽어옴.',curItem.file);
        
        handlers[curItem.method] = new jayson.Method({
           handler:curHandler,
           collect: true,
            params: Array
        });
        
        console.log('메소드 [%s]이(가) 핸들러로 추가됨.',curItem.method);
    }
    
    var jaysonServer = jayson.server(handlers);
    
    console.log('패스 ['+api_path+']에서 RPC 호출을 라우팅하도록 설정함.');
    
    app.post(api_path,function(req,res,next){
        console.log('패스 ['+api_path+']에서 JSON-RPC 호출됨.');
        
        var option = {};
        
        var contentType = req.headers['content-type'] ||'';
        if(!RegExp('application/json','i').test(contentType)){
            console.log('application/json 타입이 아님.');
            return error(415);
        };
        
        if(!req.body||typeof(req.body)!=='object'){
            console.log('요청 body가 비정상임.');
            return error(400,'Request body must be parsed');
        }
        
        console.log('RPC 함수를 호출합니다.');
        jaysonServer.call(req.body,function(error,success){
            var response = error || success;
            
            console.log(response);
            
            utils.JSON.stringify(response,option,function(err,body){
                if(err) return err;
                
                if(body){
                    var headers = {
                        "Content-Length": Buffer.byteLength(body,'utf-8'),
                        "Content-Type":"application/json"
                    };
                    res.writeHead(200,headers);
                    res.write(body);
                }else{
                    res.writeHead(204);
                }
                res.end();
            });
        });
        
        function error(code,headers){
            res.writeHead(code,headers || {});
            res.end();
        }
        
    });
    
    return handlers;
}

module.exports = handler_loader;