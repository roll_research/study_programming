# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 12:32:06 2020

@author: GM190524
"""

import pandas as pd
import matplotlib.pyplot as plt

plt.style.use('classic')

df = pd.read_csv('./auto-mpg.csv',header=None)

df.columns = ['mpg','cylinders','displacement','horespower','weight',
              'acceleration','model year','origin','name']

df.plot(kind='scatter',x='weight',y='mpg',c='coral',s=10,figsize=(10,5))
plt.title('Scatter Plot - mpg vs weight')
plt.show()
