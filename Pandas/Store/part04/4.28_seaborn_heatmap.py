# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 16:14:53 2020

@author: GM190524
"""

import matplotlib.pyplot as plt
import seaborn as sns

titanic = sns.load_dataset('titanic')

table = titanic.pivot_table(index=['sex'],columns=['class'],aggfunc='size')

sns.heatmap(table,
            annot=True,fmt='d',
            cmap='YlGnBu',
            linewidth=5,
            cbar=True)

plt.show()