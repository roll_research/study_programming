# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 17:58:38 2020

@author: GM190524
"""

import seaborn as sns

titanic = sns.load_dataset('titanic')
df = titanic.loc[:,['age','fare']]
df['ten'] = 10
print(df.head())
print()

def add_two_obj (a,b):
    return a+b

df['add'] = df.apply(lambda x : add_two_obj(x['age'],x['fare']),axis=1)
print(df.head())