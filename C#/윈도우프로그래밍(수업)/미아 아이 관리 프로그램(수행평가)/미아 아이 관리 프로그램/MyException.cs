﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 미아_아이_관리_프로그램
{
    class MyException
    {
        public void select_exception()
        {
            MessageBox.Show("잘못된 부분이 있습니다. 수정 부탁드립니다.", "알림");
        
        }
        public void select_exception_phone_number()
        {
            MessageBox.Show("핸드폰 입력이 초과되었습니다.","알림");
        }
        public void select_exception_age()
        {
            MessageBox.Show("경찰서에 신고하시기 바랍니다.","알림");
        }
    }
}
