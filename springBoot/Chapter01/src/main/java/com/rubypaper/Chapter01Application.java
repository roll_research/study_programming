package com.rubypaper;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.rubypaper","com.ruby"})
public class Chapter01Application {

	public static void main(String[] args) {
		SpringApplication applicaion = new SpringApplication(Chapter01Application.class);
		applicaion.setWebApplicationType(WebApplicationType.NONE);
		applicaion.setBannerMode(Banner.Mode.OFF);
		applicaion.run(args);
		//SpringApplication.run(Chapter01Application.class, args);
	}

}
