# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 14:15:02 2020

@author: GM190524
"""

import pandas as pd

df = pd.read_csv('stock-data.csv')

df['new_Date'] = pd.to_datetime(df['Date'])
df.set_index('new_Date',inplace=True)

print(df.head())
print()
print(df.index)

df_y = df['2018']
print(df_y.head())
print()

df_ym = df.loc['2018-07']
print(df_ym)
print()

df_ym_cols = df.loc['2018-07','Start':'High']
print(df_ym_cols)
print()

df_ymd = df['2018-07-02']
print(df_ymd)
print()

df_ymd_range = df['2018-06-25':'2018-06-20']
print(df_ymd_range)

today = pd.to_datetime('2018-12-25')
df['time_delta'] = today - df.index
print(df)
df.set_index('time_delta',inplace=True)
df_180 = df['180 days':'189 days']
print(df_180)

