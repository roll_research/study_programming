﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            productBindingSource.Add(new Product() { Name = "감자", price = 500 });
            productBindingSource.Add(new Product() { Name = "사과", price = 700 });
            productBindingSource.Add(new Product() { Name = "바나나", price = 2000 });

            comboBox1.SelectedIndexChanged += DataSelect;
            listBox1.SelectedIndexChanged += DataSelect;
        }


        private void DataSelect(object sender, EventArgs e)
        {
            if(sender is ComboBox)
            {
                ComboBox comboBox = (ComboBox)sender;
                Product product = (Product)comboBox.SelectedItem;
                MessageBox.Show(comboBox.SelectedValue.ToString());
                MessageBox.Show(product.Name + ":" + product.price);
            }else if (sender is ListBox)
            {
                ListBox listbox = (ListBox)sender;
                Product product = (Product)listbox.SelectedItem;
                MessageBox.Show(listbox.SelectedValue.ToString());
                MessageBox.Show(product.Name + ":" + product.price);
            }
        }
    }
}
