﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine(52 + 273);
            Console.WriteLine(5 + 3 + 2);
            Console.WriteLine(10 % 5);
            Console.WriteLine(7 % 3);
            */
            /*
            Console.WriteLine(1 + 2);
            Console.WriteLine(1 - 2);
            Console.WriteLine(1 * 2);
            Console.WriteLine(1 / 2);
            Console.WriteLine(1 % 2);
            */
            /*
            Console.WriteLine(4 % 3);
            Console.WriteLine(-4 % 3);
            Console.WriteLine(4 % -3);
            Console.WriteLine(-4 % -3);
            */
            /*
            Console.WriteLine(52.273);
            Console.WriteLine(0);
            Console.WriteLine(0.0);
            */

            /*
            Console.WriteLine(1.0 + 2.0);
            Console.WriteLine(1.0 - 2.0);
            Console.WriteLine(1.0 * 2.0);
            Console.WriteLine(1.0 / 2.0);
            Console.WriteLine(5.0 % 2.2);
            */

            /*
            Console.WriteLine('A');
            Console.WriteLine('가');
            Console.WriteLine('나');
            */

            //Console.WriteLine("안녕하세요." + "안녕");

            /*
            Console.WriteLine("가나다" + "라마" + "바사아" + "자차카타" + "파하");

            Console.WriteLine("안녕하세요"[0]);
            Console.WriteLine("안녕하세요"[1]);
            Console.WriteLine("안녕하세요"[3]);

            Console.WriteLine('가' + '힣');
            */

            /*
            Console.WriteLine(true);
            Console.WriteLine(false);

            Console.WriteLine(52 < 273);
            Console.WriteLine(52 > 273);
            */

            /*
            Console.WriteLine(!true);
            Console.WriteLine(!false);
            Console.WriteLine(!(52 < 273));
            Console.WriteLine(!(52 > 273));
            */

            /*
            int number = 10;
            Console.WriteLine(3 < number&& number < 8);
            */

            /*
            Console.WriteLine(DateTime.Now.Hour < 3 || 8 < DateTime.Now.Hour);
            Console.WriteLine(3 < DateTime.Now.Hour && DateTime.Now.Hour < 8);
            */

            /*
            int a = 273;
            int b = 52;

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            Console.WriteLine(a % b);
            */

            /*
            int a = 2147483640;
            int b = 52273;

            Console.WriteLine(a + b);
            */
            /*
            int a = 2000000000;
            int b = 1000000000;

            Console.WriteLine((long)a + b);
            Console.WriteLine(a + (long)b);
            Console.WriteLine((long)a + (long)b);
            */

            /*
            Console.WriteLine(int.MinValue);
            Console.WriteLine(int.MaxValue);

            Console.WriteLine(123456 + 65432L);
            */

            /*
            double a = 52.273;
            double b = 103.32;

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            */

            /*
            char a = 'a';
            Console.WriteLine(a);
            Console.WriteLine(sizeof(int));
            Console.WriteLine(sizeof(char));
            Console.WriteLine(sizeof(double));
            Console.WriteLine(sizeof(float));
            Console.WriteLine(sizeof(long));
            */

            /*
            char a = 'a';
            char b = 'b';

            Console.WriteLine(a + b);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);
            Console.WriteLine(a / b);
            Console.WriteLine(a % b);
            */
            /*
            string message = "안녕하세요";

            Console.WriteLine(message + "!");
            Console.WriteLine(message[0]);
            Console.WriteLine(message[1]);
            Console.WriteLine(message[2]);
            */

            //Console.WriteLine("string :" + sizeof(String));

            /*
            bool one = 10 < 0;
            bool other = 20 > 100;

            Console.WriteLine(one);
            Console.WriteLine(other);
            */

            /*
            int output = 0;
            output += 52;
            output += 273;
            output += 103;

            Console.WriteLine(output);
            */

            /*
            string output = "hello ";
            output += "world ";
            output += "!";

            Console.WriteLine(output);
            */

            /*
            int number = 10;
            number++;
            Console.WriteLine(number);
            number--;
            Console.WriteLine(number);
            */

            /*
            int number = 10;
            Console.WriteLine(number);
            Console.WriteLine(number++);
            Console.WriteLine(number--);
            Console.WriteLine(number);

            Console.WriteLine(number);
            Console.WriteLine(++number);
            Console.WriteLine(--number);
            Console.WriteLine(number);
            */
            /*
            int _int = 273;
            long _long = 52273103265;
            float _float = 52.273F;
            double _double = 52.273;
            char _char = '글';
            string _string = "문자열";

            Console.WriteLine(_int.GetType());
            Console.WriteLine(_long.GetType());
            Console.WriteLine(_float.GetType());
            Console.WriteLine(_double.GetType());
            Console.WriteLine(_char.GetType());
            Console.WriteLine(_string.GetType());
            */
            /*
            Console.WriteLine((273).GetType());
            Console.WriteLine((522731033265L).GetType());
            Console.WriteLine((52.273F).GetType());
            Console.WriteLine(('자').GetType());
            Console.WriteLine(("문자열").GetType());

            var num = 584;
            num = "변경";
            */

            //var number = 20;
            /*
            var numberA = 100L;
            var numberB = 100.0;
            var numberC = 100.0F;
            */

            /*
            string input=Console.ReadLine();
            Console.WriteLine("input: " + input);
            */

            /*
            long longNumber = 2147483647L + 2147483647L;
            int intNumber = (int)longNumber;
            Console.WriteLine(intNumber);
            */

            /*
            var a = (int)10.0;
            var b = (float)10;
            var c = (double)10;
            */

            /*long longNumber = 2147483647L + 2147483647L;
            int intNumber = (int)longNumber;
            Console.WriteLine(intNumber);
            */
            /*
            long longNumber = 52273;
            int intNumber = (int)longNumber;
            Console.WriteLine(intNumber);
            */

            /*
            int intNumber = 2147483647;

            long intToLong = intNumber;
            Console.WriteLine(intToLong);

            double intToDouble = intNumber;
            Console.WriteLine(intToDouble);
            */

            /*
            string numberstring = "52273";
            int intNumber = int.Parse(numberstring);
            Console.WriteLine(intNumber);
            */

            /*
            Console.WriteLine(int.Parse("52"));
            Console.WriteLine(long.Parse("273"));
            Console.WriteLine(float.Parse("52.273"));
            Console.WriteLine(double.Parse("103.32"));

            Console.WriteLine(int.Parse("52").GetType());
            Console.WriteLine(long.Parse("273").GetType());
            Console.WriteLine(float.Parse("52.273").GetType());
            Console.WriteLine(double.Parse("103.32").GetType());
            Console.WriteLine(("안녕").GetType());
            */

            /*
            Console.WriteLine((10).ToString().GetType());
            Console.WriteLine((true).ToString().GetType());
            Console.WriteLine((false).ToString().GetType());
            */
            /*
            double number = 52.273103;
            Console.WriteLine(number.ToString("0.0"));
            Console.WriteLine(number.ToString("0.00"));
            Console.WriteLine(number.ToString("0.000"));
            */
            /*
            Console.WriteLine(52 + 273);
            Console.WriteLine("52" + 273);
            Console.WriteLine(52 + "273");
            Console.WriteLine("52" + "273");
            */
            /*
            int number = 52273;
            string outputA = number + "";
            Console.WriteLine(outputA.GetType());

            char character = 'a';
            string outputB = character + "";
            Console.WriteLine(outputB.GetType());
            */
            /*
            Console.WriteLine(bool.Parse("true"));
            Console.WriteLine(bool.Parse("false"));
            Console.WriteLine(bool.Parse("True"));
            Console.WriteLine(bool.Parse("False"));
            */

            /*int output = int.MinValue;
            Console.WriteLine("  %%%%%" + "\\::");
            */
        }
    }

 
}
 