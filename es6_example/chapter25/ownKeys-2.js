let sportsObj = Object.defineProperties({},{
    baseball:{value:"축구",enumberable:true},
    swim:{value:"수영"}
});

let newProxy = new Proxy(sportsObj,{
    ownKeys(target){
        return Object.keys(target);
    }
});

console.log(Object.keys(newProxy));