﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 로또_당첨기
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
           
        }
    }
     class impormation
    {
        string name;

        public virtual void set_name(string name)
        {
            this.name = name;
        }
        public virtual string get_name()
        {
            return this.name;
        }

        string age;
        public virtual void set_age(string age)
        {
            this.age = age;
        }
        public virtual string get_age()
        {
            return this.age;
        }

        string phone_number;
        public virtual void set_phone_number(string phone_number)
        {
            this.phone_number = phone_number;
        }
        public virtual string get_phone_number()
        {
            return this.phone_number;
        }

        string address;
        public virtual void set_addres(string address)
        {
            this.address = address;
        }
        public virtual string get_address()
        {
            return this.address;
        }

        public virtual string action() {
            return null;
        }

    }

    class boy : impormation
    {

        public boy(string name, string age, string phone_number, string address)
        {
            base.set_name(name);
            base.set_age(age);
            base.set_phone_number(phone_number);
            base.set_addres(address);
        }


        public override string action()
        {
            return "남자 아이를 찾고 있습니다.";
        }
    }

    class girl : impormation
    {

        public girl(string name, string age, string phone_number, string address)
        {
            base.set_name(name);
            base.set_age(age);
            base.set_phone_number(phone_number);
            base.set_addres(address);
        }


        public override string action()
        {
            return "여자 아이를 찾고 있습니다.";
        }
    }

    class protected_person : impormation
    {

        public protected_person(string name, string age, string phone_number, string address)
        {
            base.set_name(name);
            base.set_age(age);
            base.set_phone_number(phone_number);
            base.set_addres(address);
        }


        public override string action()
        {
            return "장애 아동을 찾고 있습니다.";
        }
    }
}
