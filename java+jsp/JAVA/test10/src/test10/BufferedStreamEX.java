package test10;

import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BufferedStreamEX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BufferedOutputStream bs = null;
		
		try{
			bs = new BufferedOutputStream(new FileOutputStream("C:/save_depot/save_micro_number.txt"));
			String str = "오늘 날씨는 아주 좋습니다.";
			bs.write(str.getBytes());
			
			bs.close();
			
			String filePath = "C:/save_depot/save_micro_number.txt";
			FileInputStream fileStream =null;
			
			fileStream = new FileInputStream(filePath);
			
			byte[] readBuffer = new byte[fileStream.available()];
			while(fileStream.read(readBuffer)!=-1){}
			System.out.println(new String(readBuffer));
			String roll = new String(readBuffer);
			System.out.println(roll);
			
			fileStream.close();
			
		}catch(Exception e){
			e.getStackTrace();
		}finally{
			try {
				bs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
