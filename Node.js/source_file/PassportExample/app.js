var express = require('express')
, http = require('http')
, path = require('path');

var bodyParser = require('body-parser')
,cookieParser = require('cookie-parser')
,session = require('express-session')
,static = require('serve-static')
,errorHandler = require('errorhandler');

var expressErrorHandler = require('express-error-handler');
var expressSession = require('express-error-handler');

var mongoose = require('mongoose');

var crypto = require('crypto');

var database_loader = require('./database/database_loader');
var route_loader = require('./routes/route_loader');
var config = require('./config');

var app = express();

app.set('port',process.env.PORT||3000);

app.use(bodyParser.urlencoded({ extended: false}));

app.use(bodyParser.json());

app.use('/public',static(path.join(__dirname,'public')));

app.use(cookieParser());

app.use(expressSession({
    secret:'my key',
    resave:true,
    saveUninitialized:true
}));

app.use(session({
    cookie :{maxAge:6000},
    secret : 'my key',
    resave : false,
    saveUninitialized:false
}));

app.set('views',__dirname+'/views');

app.set('view engine','ejs');

console.log('뷰 엔진이 ejs로 설정되었습니다.');

var passport = require('passport');
var flash = require('connect-flash');
var LocalStrategy = require('passport-local').Strategy;


app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

passport.serializeUser(function(user,done){
    console.log('serializeUser() 호출됨');
    console.dir(user);
    done(null,user);
});

passport.deserializeUser(function(user,done){
    console.log('deserializeUser() 호출됨');
    console.dir(user);
    
    done(null,user);
});

passport.use('local-login', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
},function(req,email,password,done){
    console.log('passport의 local-login 호출됨 : '+ email+', '+password);
    
    var database = app.get('database');
    database.UserModel.findOne({'email':email},function(err,user){
        if(err) {return done(err);}
        
        if(!user){
            console.log('계정이 일치하지 않음.');
            return done(null,false,req.flash('loginMessage','등록된 계정이 없습니다.'));
        }
        
        var authenticated = user.authenticate(password,user._doc.salt,user._doc.hashed_password);
        
        if(!authenticated){
            console.log('비밀번호 일치하지 않음.');
            return done(null,false,req.flash('loginMessage','비밀번호가 일치하지 않습니다.'));
        }
        
        console.log('계정과 비밀번호가 일치함');
        return done(null,user);
    });
}));

passport.use('local-signup',new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
}, function(req,email,password,done){
    var paraName = req.body.name || req.query.name;
    console.log('passport의 local-signup 호출됨 :'+email+', '+password+', '+paraName);
    
    process.nextTick(function(){
        var database = app.get('database');
        database.UserModel.findOne({'email':email},function(err,user){
            if(err){
                return done(err);
            }
            if(user){
                console.log('기존에 계정이 있음');
                return done(null,false,req.flash('signupMessage','계정이 이미 있습니다.'));
            }else{
                var user = new database.UserModel({'email':email,'password':password,
                                                  'name':paraName});
                user.save(function(err){
                    if(err) {throw err;}
                    console.log('사용자 데이터 추가함');
                    return done(null,user);
                });
            }
        });
    })
}));


var router = express.Router();
route_loader.init(app,router);

router.route('/').get(function(req,res){
    console.log('/ 패스 요청함');
    res.render('index.ejs');
});

app.get('/login',function(req,res){
    console.log('/login 패스 요청됨');
    res.render('login.ejs',{message : req.flash('loginMessage')});
})

app.post('/login',passport.authenticate('local-login',{
    successRedirect : '/profile',
    failureRedirect : '/login',
    failureFlash : true
}));

app.get('/signup',function(req,res){
    console.log('/signup 패스 요청됨.');
    res.render('signup.ejs',{message:req.flash('signupMessage')});
});

app.post('/signup',passport.authenticate('local-signup',{
    successRedirect : '/profile',
    failureRedirect : '/signup',
    failureFlash : true
}));

router.route('/profile').get(function(req,res){
    console.log('/profile 패스 요청됨.');
    
    console.log('req.user 객체의 값');
    console.dir(req.user);
    
    if(!req.user){
        console.log('사용자 인증이 안 된 상태임');
        res.redirect('/');
        return;
    }
    
    console.log('사용자 인증된 상태임');
    if(Array.isArray(req.user)){
       res.render('profile.ejs',{user:req.user[0]._doc});
       }else{
           res.render('profile.ejs',{user:req.user});
       }
});

app.get('/logout',function(req,res){
    console.log('/logout 패스 요청됨');
    req.logout();
    res.redirect('/');
});

var errorHandler = expressErrorHandler({
    static:{
        '404':'./public/404.html'
    }
});

app.use(expressErrorHandler.httpError(404));
app.use(errorHandler);

http.createServer(app).listen(app.get('port'),function(){
    console.log('서버가 시작되었습니다. 포트 :'+ app.get('port'));
    
    database_loader.init(app,config);
});