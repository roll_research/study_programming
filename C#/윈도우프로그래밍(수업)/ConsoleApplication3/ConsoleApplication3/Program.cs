﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class piramide
    {
        int hight;
        int select;


        public void set_hight(string hight)
        {
            this.hight = int.Parse(hight);
        }
        public void set_select(string select)
        {
            this.select = int.Parse(select);
        }

        public void draw()
        {
            switch (select)
            {
                case 1:
                    star_a();
                    break;
                case 2:
                    star_b();
                    break;
                case 3:
                    star_c();
                    break;
                default:
                    Console.WriteLine("잘못된 경우입니다.");
                    break;
            }
        }

        public void star_a()
        {
            for(int i =0; i < this.hight; i++)
            {
                for (int j = 0; j < i + 1; j++)
                    Console.Write('*');
                Console.Write('\n');
            }
        }
        public void star_b()
        {
            for(int i=0; i<this.hight; i++)
            {
                for (int j = 0; j < this.hight - i; j++)
                    Console.Write(' ');
                for(int j = 0; j < i + 1; j++)
                {
                    Console.Write('*');
                }
                Console.Write('\n');
            }
        }
        public void star_c()
        {

        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            piramide pramide = new piramide();
            Console.WriteLine("높이와 선택범위를 선택해주세요.");
           string high=Console.ReadLine();
           string choice = Console.ReadLine();
            pramide.set_hight(high);
            pramide.set_select(choice);
            pramide.draw();
           
        }
    }
}
