import java.util.Scanner;


public class MainPolder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Animal dog = new Dog();
		Animal cat = new Cat();
		Animal bird = new Bird();
		
		
		Scanner scan = new Scanner(System.in);
		
		//이름, 성별, 나이, 짖는법, 걷는법, 먹는 것;
		System.out.println("개의 정보를 입력합니다.(순서별 => 이름,성별,나이,짖는법,걷는법,먹는것)");
		dog.setName(scan.nextLine());
		dog.setSexual(scan.nextLine());
		dog.setAge(Integer.parseInt(scan.nextLine()));
		dog.setBark(scan.nextLine());
		dog.setWark(scan.nextLine());
		dog.setEat(scan.nextLine());
		
		System.out.println("고양이의 정보를 입력합니다.(순서별 => 이름,성별,나이,짖는법,걷는법,먹는것)");
		cat.setName(scan.nextLine());
		cat.setSexual(scan.nextLine());
		cat.setAge(Integer.parseInt(scan.nextLine()));
		cat.setBark(scan.nextLine());
		cat.setWark(scan.nextLine());
		cat.setEat(scan.nextLine());
		
		System.out.println("새의 정보를 입력합니다.(순서별 => 이름,성별,나이,짖는법,걷는법,먹는것)");
		bird.setName(scan.nextLine());
		bird.setSexual(scan.nextLine());
		bird.setAge(Integer.parseInt(scan.nextLine()));
		bird.setBark(scan.nextLine());
		bird.setWark(scan.nextLine());
		bird.setEat(scan.nextLine());
		
		System.out.println("정보를 출력합니다.");
		
		dog.information();
		cat.information();
		bird.information();
		
	}
}
