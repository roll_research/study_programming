# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 13:22:13 2020

@author: GM190524
"""

import pandas as pd

df= pd.read_csv('stock-data.csv')

print(df.head())
print()
print(df.info())

df['new_Date'] = pd.to_datetime(df['Date'])

print(df.head())
print()
print(df.info())
print()
print(type(df['new_Date'][0]))

df.set_index('new_Date',inplace=True)
df.drop('Date',axis=1,inplace=True)

print(df.head())
print()
print(type(df.info()))