# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 15:03:56 2020

@author: GM190524
"""

import seaborn as sns

df = sns.load_dataset('titanic')

print(df['age'].head(10))
print()

median_age = df['age'].median(axis=0)
df['age'].fillna(median_age,inplace=True)

#mean_age = df['age'].mean(axis=0)
#df['age'].fillna(mean_age,inplace=True)

print(df['age'].head(10))

