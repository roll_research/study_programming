package com.springbook.biz;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("tv")
public class LgTV implements TV{
	
	/*
	@Autowired
	@Qualifier("apple")
	*/
	//@Resource(name = "apple")
	@Autowired
	private Speaker speaker;
	
	public LgTV() {
		// TODO Auto-generated constructor stub
		System.out.println("===> LgTV 객체 생성");
	}
	
	@Override
	public void powerOn() {
		// TODO Auto-generated method stub
		System.out.println("LgTV -- 전원 켠다.");
	}
	@Override
	public void powerOff() {
		// TODO Auto-generated method stub
		System.out.println("LgTv -- 전원 끈다.");
	}
	@Override
	public void volumUp() {
		// TODO Auto-generated method stub
		speaker.volumeUp();
	}
	@Override
	public void volumDown() {
		// TODO Auto-generated method stub
		speaker.volumeDown();
	}
}
