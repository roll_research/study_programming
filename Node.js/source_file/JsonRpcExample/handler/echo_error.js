var echo_error = function(params,callback){
    console.log('JSON-RPC echo_error 호출됨.');
    console.dir(params);
    
    if(params.length <2){
        callback({
            code : 400,
            message : 'Insufficient parameters'
        },null);
        return;
    }
    
    var output = 'Success';
    callback(null,output);
};

module.exports = echo_error;