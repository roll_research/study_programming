public class Book00 {

	String title;
	String author;
	int ISBN;
	
	public Book00(String title, String author, int ISBN){
		this.title = title;
		this.author = author;
		this.ISBN = ISBN;
	}
	
	public Book00(String title, int ISBN){
		this(title,"Anonymous",ISBN);
	}
	public Book00(){
		this(null,null,0);
		System.out.println("생성자가 호출되었음");
	}
	
	public static void main(String []args){
		Book00 javaBook = new Book00("Java JDK","황기태",3333);
		Book00 holyBile = new Book00("Holy Bile",1);
		Book00 emptyBook = new Book00();
	}
}
